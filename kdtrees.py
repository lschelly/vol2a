# kdtrees.py
"""Volume 2A: Data Structures 3 (K-d Trees).
Logan Schelly
Math 321
19 October 2016
"""

import numpy as np

# Problem 1
def metric(x, y):
	"""Return the euclidean distance between the 1-D arrays 'x' and 'y'.

	Raises:
		ValueError: if 'x' and 'y' have different lengths.

	Example:
		>>> metric([1,2],[2,2])
		1.0
		>>> metric([1,2,1],[2,2])
		ValueError: Incompatible dimensions.
	"""
	if x.ndim != 1:
		raise ValueError("%r is not a 1-D array"%x)
	elif y.ndim != 1:
		raise ValueError("%r is not a 1-D array"%y)
	elif len(x) != len(y):
		raise ValueError("len(%r) = %d != %d = len(%r)"%(x, len(x), len(y), y))
	else:
		return np.sqrt(sum( (x-y)**2 ))


# Problem 2
def exhaustive_search(data_set, target):
	"""Solve the nearest neighbor search problem exhaustively.
	Check the distances between 'target' and each point in 'data_set'.
	Use the Euclidean metric to calculate distances.

	Inputs:
		data_set ((m,k) ndarray): An array of m k-dimensional points.
		target ((k,) ndarray): A k-dimensional point to compare to 'dataset'.

	Returns:
		((k,) ndarray) the member of 'data_set' that is nearest to 'target'.
		(float) The distance from the nearest neighbor to 'target'.
	"""
	nearest_neighbor = data_set[0]
	closest_distance = metric(nearest_neighbor, target)
	
	for neighbor in data_set:
	
		distance_to_neighbor = metric(neighbor, target)
		
		if(distance_to_neighbor < closest_distance):
			nearest_neighbor = neighbor
			closest_distance = distance_to_neighbor
	
	return nearest_neighbor, closest_distance


#Copy the BST and BSTNode Classes
class BSTNode(object):
	"""A Node class for Binary Search Trees. Contains some data, a
	reference to the parent node, and references to two child nodes.
	"""
	def __init__(self, data):
		"""Construct a new node and set the data attribute. The other
		attributes will be set when the node is added to a tree.
		"""
		self.value = data
		self.prev = None		# A reference to this node's parent node.
		self.left = None		# self.left.value < self.value
		self.right = None	   # self.value < self.right.value


class BST(object):
	"""Binary Search Tree data structure class.
	The 'root' attribute references the first node in the tree.
	"""
	def __init__(self):
		"""Initialize the root attribute."""
		self.root = None

	def find(self, data):
		"""Return the node containing 'data'. If there is no such node
		in the tree, or if the tree is empty, raise a ValueError.
		"""

		# Define a recursive function to traverse the tree.
		def _step(current):
			"""Recursively step through the tree until the node containing
			'data' is found. If there is no such node, raise a Value Error.
			"""
			if current is None:					 # Base case 1: dead end.
				raise ValueError(str(data) + " is not in the tree.")
			if data == current.value:			   # Base case 2: data found!
				return current
			if data < current.value:				# Step to the left.
				return _step(current.left)
			else:								   # Step to the right.
				return _step(current.right)

		# Start the recursion on the root of the tree.
		return _step(self.root)

	def insert(self, data):
		"""Insert a new node containing 'data' at the appropriate location.
		Do not allow for duplicates in the tree: if there is already a node
		containing 'data' in the tree, raise a ValueError.

		Example:
			>>> b = BST()	   |   >>> b.insert(1)	 |	   (4)
			>>> b.insert(4)	 |   >>> print(b)		|	   / \
			>>> b.insert(3)	 |   [4]				 |	 (3) (6)
			>>> b.insert(6)	 |   [3, 6]			  |	 /   / \
			>>> b.insert(5)	 |   [1, 5, 7]		   |   (1) (5) (7)
			>>> b.insert(7)	 |   [8]				 |			 \
			>>> b.insert(8)	 |					   |			 (8)
		"""
		#start at root
		previous_node = self.root
		current_node = self.root
		#check to see there actually is a root
		if self.root == None:
			self.root = BSTNode(data)
			return
		
		#otherwise, we gotta go a searchin'
		done = False
		
		#find the first empty spot for data
		while (not done):
		
			previous_node = current_node
			
			if current_node != None:
				if data == current_node.value:
					raise ValueError("there is already a node containing %r"%data)
				
			if data > current_node.value:
				#this data belongs to the right
				current_node = current_node.right
				if current_node == None:
					#when this happens, we are at a leaf
					previous_node.right = BSTNode(data)
					current_node = previous_node.right
					current_node.prev = previous_node
					done = True
					
			elif data < current_node.value:
				#this data belongs tp the left
				current_node = current_node.left
				if current_node == None:
					#when this happens we are at a leaf
					previous_node.left = BSTNode(data)
					current_node = previous_node.left
					current_node.prev = previous_node
					done = True
			
		return

	def remove(self, data):
		"""Remove the node containing 'data'. Consider several cases:
			1. The tree is empty
			2. The target is the root:
				a. The root is a leaf node, hence the only node in the tree
				b. The root has one child
				c. The root has two children
			3. The target is not the root:
				a. The target is a leaf node
				b. The target has one child
				c. The target has two children
			If the tree is empty, or if there is no node containing 'data',
			raise a ValueError.

		Examples:
			>>> print(b)		|   >>> b.remove(1)	 |   [5]
			[4]				 |   >>> b.remove(7)	 |   [3, 8]
			[3, 6]			  |   >>> b.remove(6)	 |
			[1, 5, 7]		   |   >>> b.remove(4)	 |
			[8]				 |   >>> print(b)		|
		"""
		#If the tree is empty, or if there is no node containing 'data',
		#raise a ValueError.
		x = self.find(data)
		#the find method will already return a ValueError
		#if the tree is empty, or there is no node containing 'data'
		
		
		#The target is the root
		if x is self.root:
		
			#The root is the only node in the tree
			if (x.left == None) and (x.right == None):
				#just have the root point at nothing again
				self.root = None
			
			#the root has one child on the right:
			elif (x.left == None):
				#make the root be the child
				self.root = x.right
				#destroy any references to the previous root.
				self.root.prev = None
				x = None
				
			#the root has one child on the left:
			elif (x.right == None):
				#make the root be the child
				self.root = x.left
				#destroy any references to the previous root
				self.root.prev = None
				x = None
				
				
			#the root has two children
			else:
				#we must search the tree for the successor
				successor = None #(yet)
				current_node = x.right
				previous_node = x
				
				#iterate until we get a node with no left child
				while (successor == None):
					previous_node = current_node
					current_node = current_node.left
					if current_node == None:
						successor = previous_node
		
				#now we must swap the successor and the root
				
				#the easy way to do this is swap their values
				temp = x.value
				x.value = successor.value
				successor.value = temp
				#then we just delete the old successor node
				#because it now has the data to be deleted.
				#have it's parent point to it's right children
				if successor is x.right:
					x.right = successor.right
				else:
					successor.prev.left = successor.right
				#then, if it has a child, make it point to it's parent
				if successor.right != None:
					successor.right.prev = successor.prev
				#destroy any references to the succesor node, because we dumped
				#the bad data into him
				successor = None
				
		#The target is not the root
		else:
		
			#the target is a leaf node
			if (x.left == None) and (x.right == None):
				#destroy any references to the leaf
				parent = x.prev
				if parent.left is x:
					parent.left = None
				elif parent.right is x:
					parent.right = None
				
			#the target has one child on the right
			elif (x.left == None):
				#make the child point to x's parent
				child = x.right
				child.prev = x.prev
				#make x's parent point to x's child
				parent = x.prev
				if parent.left is x:
					parent.left = child
				elif parent.right is x:
					parent.right = child
				
			
			#the target has one child on the left
			elif (x.right == None):
				#make the child point to x's parent
				child = x.left
				child.prev = x.prev
				#make x's parent point to x's child
				parent = x.prev
				if parent.left is x:
					parent.left = child
				elif parent.right is x:
					parent.right = child
			
			#the target has two children
			else:
				#we must search the tree for the successor
				successor = None
				current_node = x.right
				previous_node = current_node
				
				#iterate until we get a node with no left child
				while (successor == None):
					previous_node = current_node
					current_node = current_node.left
					if current_node == None:
						successor = previous_node
						
				#now we must swap the successor and the target
				#the easy way to do this is swap their values
				temp = x.value
				x.value = successor.value
				successor.value = temp
				
				#now we make the child of the successor
				#point to the successor's parent.
				if successor.right != None:
					successor.right = successor.prev
				#make the successor's parent point to the successor's child
				if x.right is successor:
					x.right = successor.right
				else:
					successor.prev.left = successor.right
				


	def __str__(self):
		"""String representation: a hierarchical view of the BST.
		Do not modify this method, but use it often to test this class.
		(this method uses a depth-first search; can you explain how?)

		Example:  (3)
				  / \	 '[3]		  The nodes of the BST are printed out
				(2) (5)	[2, 5]	   by depth levels. The edges and empty
				/   / \	[1, 4, 6]'   nodes are not printed.
			  (1) (4) (6)
		"""

		if self.root is None:
			return "[]"
		str_tree = [list() for i in xrange(_height(self.root) + 1)]
		visited = set()

		def _visit(current, depth):
			"""Add the data contained in 'current' to its proper depth level
			list and mark as visited. Continue recusively until all nodes have
			been visited.
			"""
			str_tree[depth].append(current.value)
			visited.add(current)
			if current.left and current.left not in visited:
				_visit(current.left, depth+1)
			if current.right and current.right not in visited:
				_visit(current.right, depth+1)

		_visit(self.root, 0)
		out = ""
		for level in str_tree:
			if level != list():
				out += str(level) + "\n"
			else:
				break
		return out



# Problem 3: Write a KDTNode class.
class KDTNode(BSTNode):
	"""
	A Node class for KDT Trees.
	
	Attributes:
	
		Inherited from BST Node:
			value: MUST BE A NP ARRAY FOR THIS CLASS
			prev: "parent node"
			left: "child node" of smaller value on given axis
			right: "child node" of larger value on given axis
			
		New For KDT Node:
			axis: -- which entry of the np.array we use for comparison
	"""
	def __init__(self, data, a = 0): #have axis default to 0
		"""
		Uses same constructor as BST Node Except:
			- data must be a NumPy Array
			- we have a new attribute 'axis' which we pass in as 'a'
		"""
		#validate that data is a np.ndarray
		if isinstance(data, np.ndarray):
			pass #proceed normally
		else:
			raise TypeError("%r is not a np.ndarray"%data)
		
		#use the same constructor that BSTNode has
		BSTNode.__init__(self, data)
		
		#set axis attribute
		self.axis = a


# Problem 4: Finish implementing this class by overriding
#			the __init__(), insert(), and remove() methods.
class KDT(BST):
	"""A k-dimensional binary search tree object.
	Used to solve the nearest neighbor problem efficiently.

	Attributes:
		root (KDTNode): the root node of the tree. Like all other
			nodes in the tree, the root houses data as a NumPy array.
		k (int): the dimension of the tree (the 'k' of the k-d tree).
	"""
	def __init__(self):
		"""
		Initializes an empty KDT tree
		The root attribute is defaulted to None by the BST constructor
		the k attribute is defaulted to 0
		"""
		BST.__init__(self)
		self.k = 0

	def find(self, data):
		"""Return the node containing 'data'. If there is no such node
		in the tree, or if the tree is empty, raise a ValueError.
		"""

		# Define a recursive function to traverse the tree.
		def _step(current):
			"""Recursively step through the tree until the node containing
			'data' is found. If there is no such node, raise a Value Error.
			"""
			if current is None:					 # Base case 1: dead end.
				raise ValueError(str(data) + " is not in the tree")
			elif np.allclose(data, current.value):
				return current					  # Base case 2: data found!
			elif data[current.axis] < current.value[current.axis]:
				return _step(current.left)		  # Recursively search left.
			else:
				return _step(current.right)		 # Recursively search right.

		# Start the recursion on the root of the tree.
		return _step(self.root)

	def insert(self, data):
		"""Insert a new node containing 'data' at the appropriate location.
		Return the new node. This method should be similar to BST.insert().
		"""
		new = KDTNode(data)
		no_root_yet = (self.root == None)
		if no_root_yet:
			self.root = new
			self.k = len(data)
			self.root.axis = 0
		else:
			appropriate_location = None
			
			#iteratively search through the KD tree for the 
			#appropriate location for the new node
			current = self.root
			while appropriate_location == None:
				next = None
				ax = current.axis
				if (current.value[ax] > new.value[ax]):
					next = current.left
					if next == None:
						current.left = new
						appropriate_location = current.left
						new.axis = (current.axis + 1)%self.k
				elif (current.value[ax] < new.value[ax]):
					next = current.right
					if next == None:
						current.right = new
						appropriate_location = current.right
						new.axis = (current.axis + 1)%self.k
				else:
					raise ValueError("there is already a node containing %r"%data)
				#update for next iteration of while loop
				current = next
				
		return 	new

	def remove(*args, **kwargs):
		"""
		Disabled for KDT object
		"""
		raise NotImplementedError("The remove method is disabled for the KDT object")

# Problem 5
def nearest_neighbor(data_set, target):
	"""Use your KDT class to solve the nearest neighbor problem.

	Inputs:
		data_set ((m,k) ndarray): An array of m k-dimensional points.
		target ((k,) ndarray): A k-dimensional point to compare to 'dataset'.

	Returns:
		The point in the tree that is nearest to 'target' ((k,) ndarray).
		The distance from the nearest neighbor to 'target' (float).
	"""
	#build up the KD Tree
	kdt = KDT()
	for i in data_set:
		kdt.insert(i)
		
	def KDTsearch(current, neighbor, distance):
		"""The actual nearest neighbor search algorithm.

		Inputs:
			current (KDTNode): the node to examine.
			neighbor (KDTNode): the current nearest neighbor.
			distance (float): the current minimum distance.

		Returns:
			neighbor (KDTNode): The new nearest neighbor in the tree.
			distance (float): the new minimum distance.
		"""
		if current is None:
			return neighbor, distance
		index = current.axis
		
		if(metric(current.value, target) < distance):
			neighbor = current
			distance = metric(current.value, target)
			
		if target[index] < current.value[index]:
			neighbor, distance = KDTsearch(current.left, neighbor, distance)
			if target[index] + distance >= current.value[index]:
				neighbor, distance = KDTsearch(current.right, neighbor, distance)
		else:
			neighbor, distance = KDTsearch(current.right, neighbor, distance)
			if target[index] - distance <= current.value[index]:
				neighbor, distance = KDTsearch(current.right, neighbor, distance)
				
		return neighbor, distance
	
	root_target_distance = metric(kdt.root.value, target)	
	nearest_node, nearest_distance = KDTsearch(kdt.root, kdt.root, root_target_distance)
	return nearest_node.value, nearest_distance


# Problem 6
def postal_problem():
	"""Use the neighbors module in sklearn to classify the Postal data set
	provided in 'PostalData.npz'. Classify the testpoints with 'n_neighbors'
	as 1, 4, or 10, and with 'weights' as 'uniform' or 'distance'. For each
	trial print a report indicating how the classifier performs in terms of
	percentage of correct classifications. Which combination gives the most
	correct classifications?

	Your function should print a report similar to the following:
	n_neighbors = 1, weights = 'distance':  0.903
	n_neighbors = 1, weights =  'uniform':  0.903	   (...and so on.)
	"""
	from sklearn import neighbors
	labels, points, testlabels, testpoints = np.load("PostalData.npz").items()
	for n in [1,4,10]:
		for w in ['uniform', 'distance']:
			#Classify the testpoints
			nbrs = neighbors.KNeighborsClassifier(n_neighbors=n, weights=w, p=2)
			nbrs.fit(points[1], labels[1])
			predictions = nbrs.predict(testpoints[1])
			#Find Accuracy relative to testlabels
			a = np.average(predictions == testlabels[1])
			#report how classifier performed in terms of % correct classifications
			print "Neighbors: %i\t Weight: %r\t Accuracy: %f"%(n,w,a)

			
if __name__ == "__main__":
	want_to_test_prob1 = False
	want_to_test_prob2 = True
	want_to_test_prob3 = False
	want_to_test_prob4 = False
	want_to_test_prob5 = False
	want_to_test_prob6 = False
	
	if want_to_test_prob1:
		lotsofx = np.sin(np.linspace(0, 2*np.pi, 1000))
		lotsofy = np.cos(np.linspace(0, 2*np.pi, 1000))
		points_on_circle = np.hstack((lotsofx.reshape(-1,1), lotsofy.reshape(-1,1)))
		origin = np.array([0,0])
		for i in range(1000):
			print "Point", i
			print "The distance between the origin and this point is:"
			print metric(origin, points_on_circle[i])
			
	if want_to_test_prob2:
		from matplotlib import pyplot as plt
		
		target = np.array([0,0])
		plt.plot(target[0], target[1], 'ro', label = "Target")
		
		neighbors = np.random.rand(10,2)*2 - 1
		
		plt.plot(neighbors[: , 0], neighbors[:, 1], 'bo', label = "Neigbors")
		
		nearest_neighbor, distance = exhaustive_search(neighbors, target)
		plt.plot(nearest_neighbor[0], nearest_neighbor[1], 'go')
		
		print distance
		plt.show()
		
	if want_to_test_prob3:
	
		good_array = np.array(range(12))
		print "This is a good_array:", good_array
		print "isinstance(good_array, np.ndarray):", isinstance(good_array, np.ndarray)
		
		bad_array = "A string is an array of characters."
		print "\nThis is a bad_array:", bad_array
		print "isinstance(bad_array, np.ndarray):", isinstance(bad_array, np.ndarray)
		
		print "\nMaking a KDT Node 'x'"
		print "x = KDTNode(good_array)"
		x = KDTNode(good_array)
		print "x.value is ", x.value
		print "x.axis is ", x.axis
		
		print "\ntrying to make a KDTNode 'y'"
		print "y = KDTNode(bad_array)"
		try:
			y = KDTNode(bad_array)
		except TypeError as e:
			print "the KDTNode constructor threw a TypeError!"
			print e
		else:
			print "the KDTNode constructor FAILED to throw a TypeError."
		finally:
			print "When we try y.value we get...", y.value	
		
	if want_to_test_prob4:
		
		print "Making a KDT object, calling it mykdt."
		mykdt = KDT()
		print "mykdt.k =", mykdt.k
		
		print "\nI will insert this array:"
		array = np.array([5,5])
		print array
		mykdt.insert(array)
		print "mykdt.root.value =", mykdt.root.value
		print "mykdt.k =", mykdt.k
		print "mykdt.root.axis =", mykdt.root.axis
		
		print "\nI will now insert the following 2 arrays:"
		array1 = np.array([6,4])
		print array1
		array2 = np.array([3,2])
		print array2
		mykdt.insert(array1)
		mykdt.insert(array2)
		print "mykdt.root.right.value =", mykdt.root.right.value
		print "mykdt.root.right.axis =", mykdt.root.right.axis
		print "mykdt.root.left.value =", mykdt.root.left.value
		print "mykdt.root.left.axis =", mykdt.root.left.axis
		
		print"\nI will now insert the folloing 3 arrays:"
		array1 = np.array([9,2])
		array2 = np.array([7,7])
		array3 = np.array([2,6])
		print array1
		print array2
		print array3
		mykdt.insert(array1)
		mykdt.insert(array2)
		mykdt.insert(array3)
		print "mykdt.root.right.right.value =", mykdt.root.right.right.value
		print "mykdt.root.right.right.axis =", mykdt.root.right.right.axis
		print "mykdt.root.right.left.value =", mykdt.root.right.left.value
		print "mykdt.root.right.left.axis =", mykdt.root.right.left.axis
		print "mykdt.root.left.right.value =", mykdt.root.left.right.value
		print "mykdt.root.left.right.axis =", mykdt.root.left.right.axis
		
		print "\nNow I will try to remove", array3
		try:
			mykdt.remove(array3)
		except NotImplementedError as e:
			print "The remove method threw this NotImplementedError:"
			print e
		else:
			print "No NotImplementedError was thrown!"
		
	if want_to_test_prob5:
		from matplotlib import pyplot as plt
		
		target = np.array([0,0])
		plt.plot(target[0], target[1], 'ro', label = "Target")
		
		neighbors = np.random.rand(10,2)*2 - 1
		
		plt.plot(neighbors[: , 0], neighbors[:, 1], 'bo', label = "Neigbors")
		
		closest_neighbor, distance = nearest_neighbor(neighbors, target)
		plt.plot(closest_neighbor[0], closest_neighbor[1], 'go')
		
		print distance
		plt.show()
		
		plt.plot(target[0], target[1], 'ro', label = "Target")
		
		plt.plot(neighbors[: , 0], neighbors[:, 1], 'bo', label = "Neigbors")
		
		closest_neighbor, distance = exhaustive_search(neighbors, target)
		plt.plot(closest_neighbor[0], closest_neighbor[1], 'go')
		print distance
		plt.show()
		
	if want_to_test_prob6:
		postal_problem()
		
