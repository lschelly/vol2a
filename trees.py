# trees.py
"""Volume 2A: Data Structures II (Trees).
Logan Schelly
Math 321
11 Oct 2016
"""
import random
from matplotlib import pyplot as plt
from timeit import timeit
import numpy as np


class SinglyLinkedListNode(object):
	"""Simple singly linked list node."""
	def __init__(self, data):
		self.value, self.next = data, None

class SinglyLinkedList(object):
	"""A very simple singly linked list with a head and a tail."""
	def __init__(self):
		self.head, self.tail = None, None
	def append(self, data):
		"""Add a Node containing 'data' to the end of the list."""
		n = SinglyLinkedListNode(data)
		if self.head is None:
			self.head, self.tail = n, n
		else:
			self.tail.next = n
			self.tail = n

def iterative_search(linkedlist, data):
	"""Search 'linkedlist' iteratively for a node containing 'data'.
	If there is no such node in the list, or if the list is empty,
	raise a ValueError.

	Inputs:
		linkedlist (SinglyLinkedList): a linked list.
		data: the data to search for in the list.

	Returns:
		The node in 'linkedlist' containing 'data'.
	"""
	current = linkedlist.head
	while current is not None:
		if current.value == data:
			return current
		current = current.next
	raise ValueError(str(data) + " is not in the list.")

# Problem 1
def recursive_search(linkedlist, data):
	"""Search 'linkedlist' recursively for a node containing 'data'.
	If there is no such node in the list, or if the list is empty,
	raise a ValueError.

	Inputs:
		linkedlist (SinglyLinkedList): a linked list object.
		data: the data to search for in the list.

	Returns:
		The node in 'linkedlist' containing 'data'.
	"""
		
	if linkedlist.head == None:
		raise ValueError(str(data) + " is not in the list.")
		
	elif linkedlist.head.value == data:
		return linkedlist.head
		
	else:
		h = linkedlist.head
		linkedlist.head = linkedlist.head.next
		try:
			node = recursive_search(linkedlist, data)
		finally:
			linkedlist.head = h
		return node


class BSTNode(object):
	"""A Node class for Binary Search Trees. Contains some data, a
	reference to the parent node, and references to two child nodes.
	"""
	def __init__(self, data):
		"""Construct a new node and set the data attribute. The other
		attributes will be set when the node is added to a tree.
		"""
		self.value = data
		self.prev = None		# A reference to this node's parent node.
		self.left = None		# self.left.value < self.value
		self.right = None	   # self.value < self.right.value


class BST(object):
	"""Binary Search Tree data structure class.
	The 'root' attribute references the first node in the tree.
	"""
	def __init__(self):
		"""Initialize the root attribute."""
		self.root = None

	def find(self, data):
		"""Return the node containing 'data'. If there is no such node
		in the tree, or if the tree is empty, raise a ValueError.
		"""

		# Define a recursive function to traverse the tree.
		def _step(current):
			"""Recursively step through the tree until the node containing
			'data' is found. If there is no such node, raise a Value Error.
			"""
			if current is None:					 # Base case 1: dead end.
				raise ValueError(str(data) + " is not in the tree.")
			if data == current.value:			   # Base case 2: data found!
				return current
			if data < current.value:				# Step to the left.
				return _step(current.left)
			else:								   # Step to the right.
				return _step(current.right)

		# Start the recursion on the root of the tree.
		return _step(self.root)

	# Problem 2
	def insert(self, data):
		"""Insert a new node containing 'data' at the appropriate location.
		Do not allow for duplicates in the tree: if there is already a node
		containing 'data' in the tree, raise a ValueError.

		Example:
			>>> b = BST()	   |   >>> b.insert(1)	 |	   (4)
			>>> b.insert(4)	 |   >>> print(b)		|	   / \
			>>> b.insert(3)	 |   [4]				 |	 (3) (6)
			>>> b.insert(6)	 |   [3, 6]			  |	 /   / \
			>>> b.insert(5)	 |   [1, 5, 7]		   |   (1) (5) (7)
			>>> b.insert(7)	 |   [8]				 |			 \
			>>> b.insert(8)	 |					   |			 (8)
		"""
		#start at root
		previous_node = self.root
		current_node = self.root
		#check to see there actually is a root
		if self.root == None:
			self.root = BSTNode(data)
			return
		
		#otherwise, we gotta go a searchin'
		done = False
		
		#find the first empty spot for data
		while (not done):
		
			previous_node = current_node
			
			if current_node != None:
				if data == current_node.value:
					raise ValueError("there is already a node containing %r"%data)
				
			if data > current_node.value:
				#this data belongs to the right
				current_node = current_node.right
				if current_node == None:
					#when this happens, we are at a leaf
					previous_node.right = BSTNode(data)
					current_node = previous_node.right
					current_node.prev = previous_node
					done = True
					
			elif data < current_node.value:
				#this data belongs tp the left
				current_node = current_node.left
				if current_node == None:
					#when this happens we are at a leaf
					previous_node.left = BSTNode(data)
					current_node = previous_node.left
					current_node.prev = previous_node
					done = True
			
		return

	# Problem 3
	def remove(self, data):
		"""Remove the node containing 'data'. Consider several cases:
			1. The tree is empty
			2. The target is the root:
				a. The root is a leaf node, hence the only node in the tree
				b. The root has one child
				c. The root has two children
			3. The target is not the root:
				a. The target is a leaf node
				b. The target has one child
				c. The target has two children
			If the tree is empty, or if there is no node containing 'data',
			raise a ValueError.

		Examples:
			>>> print(b)		|   >>> b.remove(1)	 |   [5]
			[4]				 |   >>> b.remove(7)	 |   [3, 8]
			[3, 6]			  |   >>> b.remove(6)	 |
			[1, 5, 7]		   |   >>> b.remove(4)	 |
			[8]				 |   >>> print(b)		|
		"""
		#If the tree is empty, or if there is no node containing 'data',
		#raise a ValueError.
		x = self.find(data)
		#the find method will already return a ValueError
		#if the tree is empty, or there is no node containing 'data'
		
		
		#The target is the root
		if x is self.root:
		
			#The root is the only node in the tree
			if (x.left == None) and (x.right == None):
				#just have the root point at nothing again
				self.root = None
			
			#the root has one child on the right:
			elif (x.left == None):
				#make the root be the child
				self.root = x.right
				#destroy any references to the previous root.
				self.root.prev = None
				x = None
				
			#the root has one child on the left:
			elif (x.right == None):
				#make the root be the child
				self.root = x.left
				#destroy any references to the previous root
				self.root.prev = None
				x = None
				
				
			#the root has two children
			else:
				#we must search the tree for the successor
				successor = None #(yet)
				current_node = x.right
				previous_node = x
				
				#iterate until we get a node with no left child
				while (successor == None):
					previous_node = current_node
					current_node = current_node.left
					if current_node == None:
						successor = previous_node
		
				#now we must swap the successor and the root
				
				#the easy way to do this is swap their values
				temp = x.value
				x.value = successor.value
				successor.value = temp
				#then we just delete the old successor node
				#because it now has the data to be deleted.
				#have it's parent point to it's right children
				if successor is x.right:
					x.right = successor.right
				else:
					successor.prev.left = successor.right
				#then, if it has a child, make it point to it's parent
				if successor.right != None:
					successor.right.prev = successor.prev
				#destroy any references to the succesor node, because we dumped
				#the bad data into him
				successor = None
				
		#The target is not the root
		else:
		
			#the target is a leaf node
			if (x.left == None) and (x.right == None):
				#destroy any references to the leaf
				parent = x.prev
				if parent.left is x:
					parent.left = None
				elif parent.right is x:
					parent.right = None
				
			#the target has one child on the right
			elif (x.left == None):
				#make the child point to x's parent
				child = x.right
				child.prev = x.prev
				#make x's parent point to x's child
				parent = x.prev
				if parent.left is x:
					parent.left = child
				elif parent.right is x:
					parent.right = child
				
			
			#the target has one child on the left
			elif (x.right == None):
				#make the child point to x's parent
				child = x.left
				child.prev = x.prev
				#make x's parent point to x's child
				parent = x.prev
				if parent.left is x:
					parent.left = child
				elif parent.right is x:
					parent.right = child
			
			#the target has two children
			else:
				#we must search the tree for the successor
				successor = None
				current_node = x.right
				previous_node = current_node
				
				#iterate until we get a node with no left child
				while (successor == None):
					previous_node = current_node
					current_node = current_node.left
					if current_node == None:
						successor = previous_node
						
				#now we must swap the successor and the target
				#the easy way to do this is swap their values
				temp = x.value
				x.value = successor.value
				successor.value = temp
				
				#now we make the child of the successor
				#point to the successor's parent.
				if successor.right != None:
					successor.right = successor.prev
				#make the successor's parent point to the successor's child
				if x.right is successor:
					x.right = successor.right
				else:
					successor.prev.left = successor.right
				


	def __str__(self):
		"""String representation: a hierarchical view of the BST.
		Do not modify this method, but use it often to test this class.
		(this method uses a depth-first search; can you explain how?)

		Example:  (3)
				  / \	 '[3]		  The nodes of the BST are printed out
				(2) (5)	[2, 5]	   by depth levels. The edges and empty
				/   / \	[1, 4, 6]'   nodes are not printed.
			  (1) (4) (6)
		"""

		if self.root is None:
			return "[]"
		str_tree = [list() for i in xrange(_height(self.root) + 1)]
		visited = set()

		def _visit(current, depth):
			"""Add the data contained in 'current' to its proper depth level
			list and mark as visited. Continue recusively until all nodes have
			been visited.
			"""
			str_tree[depth].append(current.value)
			visited.add(current)
			if current.left and current.left not in visited:
				_visit(current.left, depth+1)
			if current.right and current.right not in visited:
				_visit(current.right, depth+1)

		_visit(self.root, 0)
		out = ""
		for level in str_tree:
			if level != list():
				out += str(level) + "\n"
			else:
				break
		return out


class AVL(BST):
	"""AVL Binary Search Tree data structure class. Inherits from the BST
	class. Includes methods for rebalancing upon insertion. If your
	BST.insert() method works correctly, this class will work correctly.
	Do not modify.
	"""
	def _checkBalance(self, n):
		return abs(_height(n.left) - _height(n.right)) >= 2

	def _rotateLeftLeft(self, n):
		temp = n.left
		n.left = temp.right
		if temp.right:
			temp.right.prev = n
		temp.right = n
		temp.prev = n.prev
		n.prev = temp
		if temp.prev:
			if temp.prev.value > temp.value:
				temp.prev.left = temp
			else:
				temp.prev.right = temp
		if n == self.root:
			self.root = temp
		return temp

	def _rotateRightRight(self, n):
		temp = n.right
		n.right = temp.left
		if temp.left:
			temp.left.prev = n
		temp.left = n
		temp.prev = n.prev
		n.prev = temp
		if temp.prev:
			if temp.prev.value > temp.value:
				temp.prev.left = temp
			else:
				temp.prev.right = temp
		if n == self.root:
			self.root = temp
		return temp

	def _rotateLeftRight(self, n):
		temp1 = n.left
		temp2 = temp1.right
		temp1.right = temp2.left
		if temp2.left:
			temp2.left.prev = temp1
		temp2.prev = n
		temp2.left = temp1
		temp1.prev = temp2
		n.left = temp2
		return self._rotateLeftLeft(n)

	def _rotateRightLeft(self, n):
		temp1 = n.right
		temp2 = temp1.left
		temp1.left = temp2.right
		if temp2.right:
			temp2.right.prev = temp1
		temp2.prev = n
		temp2.right = temp1
		temp1.prev = temp2
		n.right = temp2
		return self._rotateRightRight(n)

	def _rebalance(self,n):
		"""Rebalance the subtree starting at the node 'n'."""
		if self._checkBalance(n):
			if _height(n.left) > _height(n.right):
				if _height(n.left.left) > _height(n.left.right):
					n = self._rotateLeftLeft(n)
				else:
					n = self._rotateLeftRight(n)
			else:
				if _height(n.right.right) > _height(n.right.left):
					n = self._rotateRightRight(n)
				else:
					n = self._rotateRightLeft(n)
		return n

	def insert(self, data):
		"""Insert a node containing 'data' into the tree, then rebalance."""
		BST.insert(self, data)
		n = self.find(data)
		while n:
			n = self._rebalance(n)
			n = n.prev

	def remove(*args, **kwargs):
		"""Disable remove() to keep the tree in balance."""
		raise NotImplementedError("remove() has been disabled for this class.")

def _height(current):
	"""Calculate the height of a given node by descending recursively until
	there are no further child nodes. Return the number of children in the
	longest chain down.

	This is a helper function for the AVL class and BST.__str__().
	Abandon hope all ye who modify this function.

								node | height
	Example:  (c)				  a | 0
			  / \				  b | 1
			(b) (f)				c | 3
			/   / \				d | 1
		  (a) (d) (g)			  e | 0
				\				  f | 2
				(e)				g | 0
	"""
	if current is None:
		return -1
	return 1 + max(_height(current.right), _height(current.left))


# Problem 4
def prob4():
	"""Compare the build and search speeds of the SinglyLinkedList, BST, and
	AVL classes. For search times, use iterative_search(), BST.find(), and
	AVL.find() to search for 5 random elements in each structure. Plot the
	number of elements in the structure versus the build and search times.
	Use log scales if appropriate.
	"""
	#----------------------------------------------
	#Get the Data
	#----------------------------------------------
	
	sll_build_times = list()
	sll_find_times = list()
	bst_build_times = list()
	bst_find_times = list()
	avl_build_times = list()
	avl_find_times = list()
	
	#read the contents of english.txt, 
	#adding the contents of each line to a list of data
	list_of_data = list()
	with open("english.txt", 'r') as f:
		list_of_data = f.readlines()
		
	#for various value of n, repeat the following;
	various_values_of_n = [2**i for i in range(3,10)]
	for n in various_values_of_n:
	
		#get a subset of random items in the list of data
		subset = random.sample(list_of_data, n)
		
		#Time how long it takes to load a singly linked list
		sll_setup = "from __main__ import SinglyLinkedListNode\n"
		sll_setup +="from __main__ import SinglyLinkedList\n"
		sll_setup +="from __main__ import iterative_search\n"
		sll_setup +="subset = %r"%subset + "\n"
		
		sll_load_cmd =  "x = SinglyLinkedList()\n"
		sll_load_cmd += "for i in subset:\n"
		sll_load_cmd += "	x.append(i)\n"
		
		t = timeit(sll_load_cmd, setup = sll_setup, number=5)/5.0
		sll_build_times.append(t)
		
		sll = SinglyLinkedList()
		for i in subset:
			sll.append(i)
		
		#Time how long it takes to load a BST
		bst_setup =  "from __main__ import BSTNode\n"
		bst_setup += "from __main__ import BST\n"
		bst_setup +="subset = %r"%subset + "\n"
		
		bst_load_cmd = "x = BST()\n"
		bst_load_cmd +="for i in subset:\n"
		bst_load_cmd +="	x.insert(i)\n"
		
		t = timeit(bst_load_cmd, setup = bst_setup, number = 5)/5.0
		bst_build_times.append(t)
		
		bst = BST()
		for i in subset:
			bst.insert(i)
		
		#Time how long it takes to load an AVL
		avl_setup = bst_setup + "from __main__ import AVL\n"
		
		avl_load_cmd = bst_load_cmd.replace("BST", "AVL")
		
		t = timeit(avl_load_cmd, setup = avl_setup, number = 5)/5
		avl_build_times.append(t)
		
		avl = AVL()
		for i in subset:
			avl.insert(i)
		
		#Find typical search times
		trial_n_find_times_sll = np.array([])
		trial_n_find_times_bst = np.array([])
		trial_n_find_times_avl = np.array([])
		
		#test on 5 random subsets
		for throwaway_index in range(5):
		
			#Choose 5 random items from the subset
			extra_setup = "import random\n"
			extra_setup +="random_5 = random.sample(subset, 5)\n"
	
			#Time how long it takes to find all 5 items in the data structure
	
			#Singly Linked List
			sll_search_setup = sll_setup + extra_setup
			sll_search_setup +="sll = SinglyLinkedList()\n"
			sll_search_setup +="for i in subset:\n"
			sll_search_setup +="	sll.append(i)\n"
			 
			sll_search_cmd ="for i in random_5:\n"
			sll_search_cmd +="	iterative_search(sll, i)\n"
	
			t = timeit(sll_search_cmd, setup = sll_search_setup, number =5)/5.0
			trial_n_find_times_sll = np.append(trial_n_find_times_sll, t)
	
			#BST
			bst_search_setup = bst_setup + extra_setup
			bst_search_setup +="bst = BST()\n"
			bst_search_setup +="for i in subset:\n"
			bst_search_setup +="	bst.insert(i)\n"
			
			bst_search_cmd = "for i in random_5:\n"
			bst_search_cmd +="	bst.find(i)\n"
			
			t = timeit(bst_search_cmd, setup = bst_search_setup, number = 5)/5.0
			trial_n_find_times_bst = np.append(trial_n_find_times_bst,t)
	
			#AVL
			avl_search_setup = avl_setup + extra_setup
			avl_search_setup +="avl = AVL()\n"
			avl_search_setup +="for i in subset:\n"
			avl_search_setup +="	avl.insert(i)\n"
			
			avl_search_cmd = bst_search_cmd.replace("bst", "avl")
			
			t = timeit(avl_search_cmd, setup = avl_search_setup, number = 5)/5.0
			trial_n_find_times_avl = np.append(trial_n_find_times_avl, t)
			
		#Append the average find time for 5 sets of 5 random things
		sll_find_times.append(trial_n_find_times_sll.mean())
		bst_find_times.append(trial_n_find_times_bst.mean())
		avl_find_times.append(trial_n_find_times_avl.mean())

	#----------------------------------------------
	#Plot the Data
	#----------------------------------------------
	#Report your findings in a single figure with two subplots:
	
	#One for build times
	plt.subplot(1,2,1)
	plt.loglog(various_values_of_n, sll_build_times, 'ro-',basex=2, label = "Singly Linked List")
	plt.loglog(various_values_of_n, bst_build_times, 'go-',basex=2, label = "BST")
	plt.loglog(various_values_of_n, avl_build_times, 'bo-',basex=2, label = "AVL")
	plt.title("Build Times")
	plt.legend(loc="upper left", prop={'size':9})
	plt.xlabel("Size of Data Structure")
	plt.ylabel("Time to Build (Seconds)")
	
	#One for search times
	plt.subplot(1,2,2)
	plt.loglog(various_values_of_n, sll_find_times, 'ro-',basex=2, label = "Singly Linked List")
	plt.loglog(various_values_of_n, bst_find_times, 'go-',basex=2, label = "BST")
	plt.loglog(various_values_of_n, avl_find_times, 'bo-',basex=2, label = "AVL")
	plt.title("Search Times")
	plt.legend(loc="upper left", prop={'size':9})
	plt.xlabel("Size of Data Structure")
	plt.ylabel("Average find time for 5 randomly chosen nodes")
	
	#Finishing touches
	plt.suptitle("Problem 4")
	plt.show()
	
	


def test_recursive_search(l, things):
	print "Making a singly linked list from ", l , "\n"
	linkedlist = SinglyLinkedList()
	
	for x in l:
		print "appending ", x
		linkedlist.append(x)
		print "linkedlist.tail.value = ", linkedlist.tail.value
		
	print "\nSearching for all these: " , things
	
	for t in things:
		print "Searching for ", t
		try:
			node = recursive_search(linkedlist, t)
			print "The recursive search returned a node with value: ", node.value
		except ValueError as e:
			print "The recursive search raised the ValueError: ", e
	
	print "\n the head of linked list has value:", linkedlist.head.value
	
if __name__ == "__main__":
	want_to_test_prob1 = False
	want_to_test_prob2 = False
	want_to_test_prob3 = False
	want_to_test_prob4 = True
	
	if want_to_test_prob1:
		l = ["hello", "world", 1, 2, 3, 4, 3.1415926, "Justin Bieber"]
		things = l + ["bananas", 1000]
		test_recursive_search(l, things)
		
	if want_to_test_prob2:
		from random import shuffle
		x = range(10)
		shuffle(x)
		bst = BST()
		for i in x:
			print "inserting ", i, "to the BST"
			bst.insert(i)
			print "This is the BST"
			print bst
		
	if want_to_test_prob3:
		from random import shuffle
		x = range(10)
		shuffle(x)
		bst = BST()
		for i in x:
			bst.insert(i)
		print "This is the bst:"
		print bst
		for i in range(10):
			print "\nremoving this from the tree:", i
			bst.remove(i)
			print "This is now the bst: "
			print bst
			
		print "\nMaking a new bst"
		bst = BST()
		for i in [5,4,7,2,6,9,1,3,8,10]:
			bst.insert(i)
		print bst
		print "removing the root."
		bst.remove(bst.root.value)
		print bst
		print "removing 9"
		bst.remove(9)
		print bst
		
	if want_to_test_prob4:
		prob4()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
