#calculator.py
"""This is for the standard_library lab
Author:Logan Schelly
Date: 6 Sept 2016"""

from numpy import sqrt

#sum
def sum(a,b):
	return a+b
	
def product(a,b):
	return a*b
	
def square_root(a):
	return sqrt(a)

