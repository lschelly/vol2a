# exceptions_fileIO.py
"""Introductory Labs: Exceptions and File I/O.
Logan Schelly
Math 345
20 September 2016
"""
def string_word_reverser(string):
	list_of_words = string.split(" ")
	reversed_words = list_of_words[::-1]
	return str(' '.join(reversed_words))
	
def transpose_wordwise(string):
	#break up into lines
	lines = string.split("\n")
	
	string_line_word_indexed = list()
	#break each line into words
	for line in lines:
		string_line_word_indexed.append(line.split(" "))
	
	the_transpose = str()
	most_words_in_line = len(max(string_line_word_indexed, key=len))
	all_the_words = [x for y in string_line_word_indexed for x in y]
	most_characters_in_a_word = len(max(all_the_words, key =len))

	
	#now access through the nth word in each line
	for word in range(most_words_in_line):
		for line in string_line_word_indexed:
			try:
				the_transpose += line[word]
				if line[word] == "":
					the_transpose += "_"*most_characters_in_a_word
				
			except IndexError:
				the_transpose += "_"*most_characters_in_a_word
			finally:
				the_transpose += " "
		the_transpose += "\n"
	
	return the_transpose

# Problem 1
def arithmagic():
	step_1 = raw_input("Enter a 3-digit number where the first and last "
	                                        "digits differ by 2 or more: ")
	#is it a number? int casting will blow up if not
	step_1_n = int(step_1)
	#is it three digits?
	if (step_1_n < 100) or (step_1_n > 999):
		raise ValueError("%r is not a three digit number" % step_1)
		return
	#do the last digits differ by 2 or more?
	first_digit = int(step_1[0])
	last_digit = int(step_1[2])
	if (abs(first_digit - last_digit) < 2):
		raise ValueError("""the first and last digits of %r 
		do not differ by 2 or more""" % step_1)
		return
		
	step_2 = raw_input("Enter the reverse of the first number, obtained "
	                                        "by reading it backwards: ")
	step_2_n = int(step_2)
	#is that the proper reversed number?
	step_1_reversed = step_1[::-1]
	if (step_1_reversed != step_2):
		raise ValueError(step_1 + " reversed is " + step_1_reversed + " not " + step_2)
		return
		
	step_3 = raw_input("Enter the positive difference of these numbers: ")
	#did they actually input the positive difference?
	step_3_n = int(step_3)
	positive_difference = abs(step_2_n - step_1_n)
	if (step_3_n != positive_difference):
		raise ValueError("The positive difference was %d, you entered %d" % (positive_difference, step_3_n)) 
		
	step_4 = raw_input("Enter the reverse of the previous result: ")
	#did they actually input the reverse?
	previous_result_reversed = step_3[::-1]
	if (step_4 != previous_result_reversed):
		raise ValueError(
		"The reverse of the previous result was %s, you entered %s" %
		(previous_result_reversed, step_4))
		
	print str(step_3) + " + " + str(step_4) + " = %i (ta-da!)" % (step_3_n + step_4_n)


# Problem 2
def random_walk(max_iters=1e12):
	from random import choice

	walk = 0
	direction = [-1, 1]
	i = long(0)
	#modify the code
	try:
		for i in xrange(int(max_iters)):
			walk += choice(direction)
	#So that if a user raises an KeyboardInterrupt...
	except KeyboardInterrupt:
		print  "Process interrupted at iteration %i." %(i)
	#If no KeyboardInterrupt is raised...
	else:
		print "Process Completed!"
	#in either case, return the walk
	finally:
		return walk
		


# Problems 3 and 4: Write a 'ContentFilter' class
class ContentFilter(object):
	"""
	ContentFilter class.  Not quite sure what it does yet.  Gonna write the constructor.
	
	I think it basically stores all the basic file information,
	and then can quickly parse/format it to be according to specs
	
	Attributes:
		name -- string denoting the name of the file
		contents -- string with THE WHOLE FREAKIN' FILE
	Methods:
		uniform -- changes all to upper or lower case and writes to new file
	"""
	def __init__(self, filename):
		"""
		Store the file name and it's contents
		
		Input: filename -- the name of the file
		
		Returns: a Content_Filter object with the file name and contents
		"""
	
		#If the filename argument is not a string, raise a TypeError. 
		if( not isinstance(filename,str)):
			raise TypeError("The filename argument was not a string")
		
		#read the file.
		try:
			f = open(filename,'r')
		
			#store the name as an attribute
			self.name = f.name
		
			#store the contents as an attribute.  Make the contents one string
			contents_with_extra_newline = f.read()
			end = len(contents_with_extra_newline)
			#for some reason the read() function appends an extra newline
			self.contents = contents_with_extra_newline[:end-1:]

		#Securely close the file
		finally:
			f.close()
		return
		
	def __str__(self):
		"""
		Returns a string containing some info about the ContentFilter
		"""
		# c.isalpha() for c in self.contents returns an array of booleans
		# the nth element of the array correspons to whether or not the nth
		#character is a letter
		num_letters = sum(c.isalpha() for c in self.contents)
		num_numbers = sum(c.isdigit() for c in self.contents)
		num_spaces = sum(c.isspace() for c in self.contents)
		num_lines = self.contents.count("\n") + 1 #we have 1 original line + all "\n"
		return """
		Source file:            %s
		Total characters:       %i
		Alphabetic characters:  %i
		Numerical characters:   %i
		Whitespace characters:  %i
		Number of lines:        %i
		"""%(self.name, len(self.contents), num_letters, num_numbers, num_spaces, num_lines)
			
	
	def uniform(self, filename, mode='w', case='upper'):
		"""
		Take a ContentFilter object, write it's contents to a file,
		but put all letters in upper case or lower case.
		
		Input: 	filename -- the name of the file you want in memory
				mode -- 'w' for write or 'a' for append.  Defaults to 'w'
				case -- 'upper' or 'lower'
				
		Output:  Nothing is returned to the program, but new file will be
		either created or overwritten in the local directory
		"""
		
		#If mode is not either 'w' or 'a', raise a 
		#ValueError with an informative message.
		if (mode != 'w') and (mode != 'a'):
			raise ValueError("parameter 'mode' can only be 'w' or 'a'")
			
		#write the data to the outfile with uniform case
		stuff_to_write_in = str()
		if case == 'upper':
			stuff_to_write_in = self.contents.upper()
		elif case == 'lower':
			stuff_to_write_in = self.contents.lower()
		else:
			raise ValueError("parameter 'case' can only be 'lower' or 'upper'")
		
		#Write the stuff and securely close the file.
		with open(filename, mode) as file_we_write_to:
			file_we_write_to.write(stuff_to_write_in)
		return
	
	def reverse(self, filename, mode='w', unit='line'):
		"""
		Take a ContentFilter object, write it's contents to a new file, but either
		reverse the order of the words in every line, or reverse the order of the lines
		
		Input:	filename -- the name of the new file you want in memory
				mode -- 'w' for write or 'a' for append.  Defaults to 'w'
				unit -- 'line' to reverse order of lines
						'word' to reverse order of words in each line
				
		"""	
		stuff_to_write_in = str()
		#If mode is not either 'w' or 'a', raise a 
		#ValueError with an informative message.
		if (mode != 'w') and (mode != 'a'):
			raise ValueError("parameter 'mode' can only be 'w' or 'a'")
			
		if( (unit != 'line') and (unit != 'word')):
			#If unit is not one of these two values, raise a ValueError.
			raise ValueError("parameter 'mode' can only be 'line' or 'word'")
			
		elif(unit == 'line'):
			list_of_lines = self.contents.split("\n")
			lines_list_reversed = list_of_lines[::-1]
			stuff_to_write_in = "\n".join(lines_list_reversed)
			
		else: #has to be 'word'
			list_of_lines = self.contents.split("\n")
			list_of_reversed_lines = list()
			for line in list_of_lines:
				 reversed_words = string_word_reverser(line)
				 list_of_reversed_lines.append(reversed_words)
			
					
			stuff_to_write_in = "\n".join(list_of_reversed_lines)
		#Now we can finally write in the stuff!
		
		#Write the stuff and securely close the file.
		with open(filename, mode) as file_we_write_to:
			file_we_write_to.write(stuff_to_write_in)
		return
		

	def transpose(self, filename, mode='w'):
		"""
		Take a ContentFilter object, write it's contents to a new file,
		but transpose the contents wordwise
		
		Input:	filename -- the name of the new file you want in memory
				mode -- 'w' for write or 'a' for append.  Defaults to 'w'
		"""
		#If mode is not either 'w' or 'a', raise a 
		#ValueError with an informative message.
		if (mode != 'w') and (mode != 'a'):
			raise ValueError("parameter 'mode' can only be 'w' or 'a'")
		
		stuff_to_write_in = transpose_wordwise(self.contents)
		
		with open(filename, mode) as file_we_write_to:
			file_we_write_to.write(stuff_to_write_in)
		return
		



if __name__ == "__main__":
	print("Making ContentFilter object x with parameter \"garbage.txt\"")
	x = ContentFilter('garbage.txt')
	print("x's name is: " + x.name + "\n")
	print("x's contents are: \n" +x.contents)
	
	try:
		notstring = 5
		print("Gonna try to pass in an integer into the constructor")
		y= ContentFilter(notstring)
	except TypeError as e:
		print("The constructor threw a TypeError:")
		print(str(e))
		if(str(e) == "The filename argument was not a string"):
			print("That was the error message we should expect")
		else:
			print("That was not the error message we should expect")
	finally:
		print "\n"
		
	print("Import Car Radio lyrics and garbage.txt")
	lyrics = ContentFilter("Car Radio lyrics.txt")
	garbage = ContentFilter("garbage.txt")
	print("\nCheck on ValueError for 'w' or 'a' on methods")
	
	print("lyrics.uniform('LYRICS.txt','x')")
	try:
		lyrics.uniform('LYRICS.txt','x')
	except ValueError:
		print("Yup, it throws a ValueError!")
		
	print("\nNow try to make a file LYRICS.txt with the lyrics in all caps")
	lyrics.uniform("LYRICS.txt")
	print("(It'll be easier if you go check that yourself)")
	print("\nNow in lower case as lyrics_lower.txt")
	lyrics.uniform("lyrics_lower.txt", 'w', 'lower')
	
	print("\nNow we'll try to pass in an invalid parameter for the case")
	try:
		lyrics.uniform("should_die.txt", 'w', 'dog')
	except ValueError as e:
		print("Uniform passed up the ValueError " + str(e))
		
	print("And finally we will see if we append properly")
	lyrics.uniform("lyrics_lower.txt", 'a', 'lower')
		
	print("\nNow onto testing the other functions")
	print("Can I reverse the lyrics line by line?")
	lyrics.reverse("lyrics_lines_reversed.txt")
	#print("What did lyrics look like when passed in?")
	#print("%r") %lyrics.contents
	lyrics.reverse("lyrics_words_reversed.txt",'w', 'word')
	print("now, finally, can I transpose?")
	print("Do I get here?")
	lyrics.transpose("I wish I could transpose a title.txt", 'w')
	easy = ContentFilter("easy_transpose_check.txt")
	easy.transpose("the_actual_transpose.txt", 'w')
	
	print "\n and now check the string magic method\n"
	print easy
	print "\n" + str(lyrics)
	print "\n" + str(garbage)
	print("hello?")
	
		
	
			
