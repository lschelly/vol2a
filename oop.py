# oop.py
"""Introductory Labs: Object Oriented Programming.
Logan Schelly
Math 321
13 Sept 2016
"""
from math import sqrt

class Backpack(object):
    """A Backpack object class. Has a name and a list of contents.

    Attributes:
        name (str): the name of the backpack's owner.
        contents (list): the contents of the backpack.
    """

    # Problem 1: Modify __init__() and put(), and write dump().
    # Make __init__() take in a color and max_size
    # have max_size default to 5
    def __init__(self, name, color, max_size = 5):
        """Set the name and initialize an empty contents list.

        Inputs:
            name (str): the name of the backpack's owner.

        Returns:
            A Backpack object wth no contents.
        """
        #store each input as an attribute
        self.name = name
        self.color = color
        self.max_size = max_size
        self.contents = []

    # Modify the put method to ensure the backpack does not go over
    # it's capacity
    def put(self, item):
        """Add 'item' to the backpack's list of contents."""
        if len(self.contents) < self.max_size:
            self.contents.append(item)
        else:
            #If the user tries to put in more than max_size items
            print("No room!")
            #and do not add the item to the contents list

    def take(self, item):
        """Remove 'item' from the backpack's list of contents."""
        self.contents.remove(item)
    """
    Add a new method called dump() that resets the contents of the backpack
    to an empty list. This method should not receive any arguments
    (except self).
    """
    def dump(self):
        self.contents = []
        
    # Magic Methods -----------------------------------------------------------

    # Problem 3: Write __eq__() and __str__().
    def __add__(self, other):
        """Add the number of contents of each Backpack. Returns int."""
        return len(self.contents) + len(other.contents)

    def __lt__(self, other):
        """Compare two backpacks. If 'self' has fewer contents
        than 'other', return True. Otherwise, return False.
        """
        return len(self.contents) < len(other.contents)
        
    def __eq__(self, other):
        """Returns True iff names, colors and number of contents are same"""
        if self.name == other.name:
            if self.color == other.color:
                if len(self.contents) == len(other.contents):
                    return True
                
        return False
        
    def __str__(self):
        """
        returns a string in the following format:
            Owner: <name>
            Color: <color>
            Size: <number of items in contents>
            Max Size: <max_size>
            Contents: [<item1>, <item2>, ...]
        """
        ret_str = "Owner:\t\t" + self.name + "\n"
        ret_str += "Color:\t\t" + self.color + "\n"
        ret_str += "Size:\t\t" + str(len(self.contents)) + "\n"
        ret_str += "Contents:\t" + str(self.contents) + "\n"
        return ret_str


# An example of inheritance. You are not required to modify this class.
class Knapsack(Backpack):
    """A Knapsack object class. Inherits from the Backpack class.
    A knapsack is smaller than a backpack and can be tied closed.

    Attributes:
        name (str): the name of the knapsack's owner.
        color (str): the color of the knapsack.
        max_size (int): the maximum number of items that can fit
            in the knapsack.
        contents (list): the contents of the backpack.
        closed (bool): whether or not the knapsack is tied shut.
    """
    def __init__(self, name, color, max_size=3):
        """Use the Backpack constructor to initialize the name, color,
        and max_size attributes. A knapsack only holds 3 item by default
        instead of 5.

        Inputs:
            name (str): the name of the knapsack's owner.
            color (str): the color of the knapsack.
            max_size (int): the maximum number of items that can fit
                in the knapsack. Defaults to 3.

        Returns:
            A Knapsack object with no contents.
        """
        Backpack.__init__(self, name, color, max_size)
        self.closed = True

    def put(self, item):
        """If the knapsack is untied, use the Backpack.put() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.put(self, item)

    def take(self, item):
        """If the knapsack is untied, use the Backpack.take() method."""
        if self.closed:
            print("I'm closed!")
        else:
            Backpack.take(self, item)


# Problem 2: Write a 'Jetpack' class that inherits from the 'Backpack' class.
class Jetpack(Backpack):
    """"
    A Jetpack object class. Inhertis from the Backpack class.
    A Jetpack has less storage space, but also has a fuel tank,
    and an ability to fly
    
    Attributes:
        name (str): the name of the Jetpack's owner.
        color (str): the color of the Jetpack.
        max_size (int): the maximum number of items that can fit
            in the Jetpack.
        contents (list): the contents of the Jetpack.
        fuel (int): the amount of fuel it has
    """
    def __init__(self, name, color, max_size = 2, fuel = 10):
        """
        Overwrite the Backpack constructor so that in addition to a name, color, and
        maximum size, it also accepts an amount of fuel. Change the default
        value of max_size to 2, and set the default value of fuel to 10.
        """
        Backpack.__init__(self, name, color, max_size)
        # Store the fuel as an attribute
        self.fuel = fuel 
        
    #add a fly method
    def fly(self, fuel_to_use):
        """
        accepts the amount of fuel to be used
        decrements fuel by that amount
        
        if there is not a fuel, there is no decrement
        """
        if self.fuel >= fuel_to_use:
            self.fuel -= fuel_to_use
        
        #if the user tries to burn more fuel than remains
        else:
            print ("Not enough fuel!")
            #and do not decrent the fuel
            
    """
    Overwrite the dump() method so that both the contents and the fuel
    tank are emptied.
    """
    def dump(self):
        """
        In addition to dumping the contents, also sets fuel = 0
        """
        Backpack.dump(self)
        self.fuel = 0    
        
# Problem 4: Write a 'ComplexNumber' class.
class ComplexNumber(object):
    """
    A complex number object class
    
    Attributes:
        real (float):       the real part of the complex number
        imaginary (float):  the imaginary part of the complex number
    """
    
    #Constructor should accept 2 numbers.
    #store the first as self.real and the second as self.imag
    def __init__(self, first_number, second_number):
        """
        Inputs: first number
                second number
        
        Returns : a complex number with the first number as the real part
        and the second number as the imaginary part
        """
        self.real = float(first_number)
        self.imag = float(second_number)
    
    #Implement a conjugate method that returns the complex conjugate
    #of the object as a new ComplexNumber object    
    def conjugate(self):
        """
        No explicit inputs.
        
        Returns a ComplexNumbers' complex conjugate as a ComplexNumber object
        """
        #remember conjugate(a+bi) = a - bi
        a = self.real
        b = self.imag
        return ComplexNumber(a, -b)
        
    #Magic methods ---------------------------------------------------
    
    
    #Absolute Value
    #Recall that abs(a+bi) == sqrt(a**2 + b**2)
    #"from math import sqrt" is on line 7
    def __abs__(self): #use the underscores to implement the built in abs() function
        a = self.real
        b = self.imag
        magnitude = sqrt(a**2 + b**2)
        return magnitude    
    
    #Comparison/Ordering (< and >)
    #Will be compared by magnitude
    #less than (<)
    def __lt__(self, other):
        magnitude_self = abs(self)
        magnitude_other = abs(other)
        return magnitude_self < magnitude_other      
    #greater than (>)
    def __gt__(self, other):
        magnitude_self = abs(self)
        magnitude_other = abs(other)
        return magnitude_self > magnitude_other       
        
    #Equality/Inequality (== and !=)
    #Complex numbers will be equal iff their real and imaginary parts are equal
    def __eq__(self, other):
        return ((self.real == other.real) and (self.imag == other.imag))
    
    def __ne__(self, other):
        return not (self == other)
        
    #Addition
    #add the real parts and add the imaginary parts
    def __add__(self, other):
        return ComplexNumber(self.real + other.real, self.imag + other.imag)
    
    #Subtraction
    #subtract the corresponding real and imaginary parts
    def __sub__(self, other):
        return ComplexNumber(self.real - other.real, self.imag - other.imag)
        
    #Multiplication
    #(a+bi) * (c+di) == ac + adi + bci + bd*i**2
    #                == ac + (ad + bc)i - bd == ac-bd + (ad+bc)i
    def __mul__(self, other):
        a = self.real
        b = self.imag
        c = other.real
        d = other.imag
        return ComplexNumber(a*c - b*d, a*d + b*c)
    
    #Division
    #(a+bi)/(c+di)  == (a+bi)(c-di)/((c+di)(c-di))
    #               == (a+bi)*conjugate(c+di)/(c**2 -cdi +cdi -d**2i**2)
    #               == (a+bi)*conjugate(c+di)/(c**2 + d**2)
    #               == (a+bi)*conjugate(c+di)/(sqrt(c**2 +d**2)**2)
    #               == (a+bi)*conjugate(c+di)/(abs(c+di)**2)
    #               == (1/(abs(c+di)**2) + 0i)*(a+bi)*(conjugate(c+di))
    #Bleh.  not working.  Try this:
    #               == ((ac + db)/(c**2 + d**2) +(bc -ad)/(c**2 + d**2)
    #Nevermind.  I was testing incorrectly.
    def __div__(self, other):
        return ComplexNumber(1/(abs(other)**2), 0) * self * other.conjugate()
        #a = self.real
        #b = self.imag
        #c = other.real
        #d = other.imag
        #return ComplexNumber((a*c + d*b)/(c**2 + d**2), (b*c - a*d)/(c**2 + d**2))
 

    
#Testing block    
if __name__ == "__main__":
    from random import randrange
    x = ComplexNumber (randrange(501),randrange(501))
    y = x.conjugate()
    z = ComplexNumber (randrange(501),randrange(501))
    
    #verification with python's complex
    x0 = x.real + 1j * x.imag
    z0 = z.real + 1j * z.imag

    print "x ==", x.real, "+", x.imag, "i"
    print "y ==", y.real, "+", y.imag, "i"
    print "z ==", z.real, "+", z.imag, "i"
    print "abs(z) ==", abs(z)
    print "sqrt(z.real**2 + z.imag**2) ==", sqrt(z.real**2 + z.imag**2)
    print "\n greater than or less than"
    print "x>y ==", x>y
    print "x<y ==", x<y
    print "x>z ==", x>z
    print "x<z ==", x<z
    print "y>x ==", y>x
    print "y<x ==", y<x
    print "y>z ==", y>z
    print "y<z ==", y<z
    print "z<x ==", z<x
    print "z>x ==", z>x
    print "\n equality and inequality"
    print "x==x ==", x==x
    print "x!=x ==", x!=x
    print "x==y ==", x==y
    print "x!=y ==", x!=y
    print "\n addition:"
    xplusy = x+y
    print "x+y ==", xplusy.real, "+", xplusy.imag, "i" 
    xplusz = x+z
    print "x+z ==", xplusz.real, "+", xplusz.imag, "i"
    print "\n subtraction:"
    xminusy = x-y
    print "x-y ==", xminusy.real, "+", xminusy.imag, "i"
    xminusz = x-z
    print "x-z ==", xminusz.real, "+", xminusz.imag, "i"
    print "\n multiplication"
    xtimesy = x*y
    print "x*y ==", xtimesy.real, "+", xtimesy.imag, "i"
    xtimesz = x*z
    print "x*z ==", xtimesz.real, "+", xtimesz.imag, "i"
    print "python says:", x0*z0
    
    print "\n division:"
    print "x/y =", (x/y).real, "+", (x/y).imag, "i"
    print "x/z =", (x/z).real, "+", (x/z).imag, "i"
    print "python says:", x0/z0
    
    mypack = Backpack("Logan", "orange")
    for x in ["apple", "banana", "coconut", "donut"]:
    	mypack.put(x)
    print mypack
    
