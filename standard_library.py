# standard_library.py
"""Introductory Labs: The Standard Library.
Logan Schelly
Math 345
6 Sept 2016
"""


# Problem 1
def prob1(l):
    """Accept a list 'l' of numbers as input and return a new list with the
    minimum, maximum, and average of the contents of 'l'.
    """
    return min(l), max(l), float(sum(l))/len(l)
    


# Problem 2
def prob2():
    """Programmatically determine which Python objects are mutable and which
    are immutable. Test numbers, strings, lists, tuples, and dictionaries.
    Print your results to the terminal.
    """
    #numbers
    x = 3
    y = x
    y+= 1
    print "Numbers are mutable: %r" %(x == y)
    
    #strings
    string1 = "abc"
    string2 = string1
    string2 += 'a'
    print "Strings are mutable: %r" %(string1 == string2)
    
    #lists
    list1 = [1,2,3]
    list2 = list1
    list2.append(1)
    print "Lists are mutable: %r" %(list1 == list2)
    
    #tuples
    tuple1 = (1,2)
    tuple2 = tuple1
    tuple2 += (1,)
    print "Tuples are mutable: %r" %(tuple1 == tuple2)
    
    #dictionaries
    dict1 = {"yes" : 1, "no" : 0}
    dict2 = dict1
    dict2[1] = 'a'
    print "Dictionaries are mutable: %r" %(dict1 == dict2)
    


# Problem 3: Create a 'calculator' module and implement this function.
def prob3(a,b):
    """Calculate and return the length of the hypotenuse of a right triangle.
    Do not use any methods other than those that are imported from your
    'calculator' module.

    Parameters:
        a (float): the length one of the sides of the triangle.
        b (float): the length the other nonhypotenuse side of the triangle.

    Returns:
        The length of the triangle's hypotenuse.
    """
    import calculator as c
    return c.square_root(c.sum(c.product(a,a),c.product(b,b)))


# Problem 4: Implement shut the box

if __name__ == "__main__":
    import numpy as np
    import sys
    import box
    
    name = ""
    #prompt user for name if not provided
    if len(sys.argv) < 2:
        name = raw_input("Please provide your name: ")
    #otherwise, the 2nd command line argument is the name
    else:
        name = sys.argv[1]
    
    
    #keep track of remaining numbers
    remaining_numbers = range(1,10)
    
    #start turn
    while(True):
        """Use the random module to simulate rolling two six-sided dice. If the sum
        of the remaining numbers is 6 or less, role only one die."""
        if sum(remaining_numbers) > 6:
            Roll = np.random.randint(1,7) + np.random.randint(1,7)
        else:
            Roll = np.random.randint(1,7)
        
        #print the remaining numbers and the sum of the dice at each turn
        print "\nNumbers left:", remaining_numbers
        print "Roll:", Roll
        
        #game ends when none of the remaining integers can be combined to the sum
        #of the dice roll
        possible_to_continue = box.isvalid(Roll, remaining_numbers)
        
            #I'm not sure isvalid handles  Roll == 1 and 1 in remaining_numbers
        possible_to_continue |= Roll in remaining_numbers
        
        if not possible_to_continue:
            break
        
        #If the game is not over, prompt the user for numbers to eliminate
        #after each dice roll
        numbers_to_eliminate = [0]
        while(True):
            numbers_to_eliminate = box.parse_input( raw_input("Numbers to elminate:") , remaining_numbers)
            if len(numbers_to_eliminate) > 0:
                if sum(numbers_to_eliminate) == Roll:
                    for i in numbers_to_eliminate:
                        remaining_numbers.remove(i)
                    break
            #if invalid input, reprompt
            print("Invalid input")
        
        #can't keep doing turns if no numbers left
        if len(remaining_numbers) == 0:
            break
        
    #End of turns while loop.  Game is over
    print "\nGame Over"
    print "Score for player " + name + ":", sum(remaining_numbers), "points"
    if sum(remaining_numbers) == 0:
	    print "Congratulations!! You shut the box!"
    
