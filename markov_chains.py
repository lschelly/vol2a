# markov_chains.py
"""Volume II: Markov Chains.
Logan Schelly
Math 321
31 October 2016
"""

import numpy as np


# Problem 1
def random_markov(n):
	"""Create and return a transition matrix for a random Markov chain with
	'n' states. This should be stored as an nxn NumPy array.
	"""
	A = np.random.random((n,n))
	#Divide each column by the sum of that column
	A /= A.sum(axis = 0)
	return A


# Problem 2
def forecast(n):
	"""Forecast tomorrow's weather given that today is hot."""
	transition = np.array([[0.7, 0.6], [0.3, 0.4]])
	#Start out assuming we have a hot day
	today = 0
	tomorrow = None # tbd
	
	#store the resulting days in a list
	days = list()
	# go through n days
	for d in range(1, n+1):
		yesterday = today #previous today is yesterday
		#print "Day %d:"%d
		#if yesterday == 0:
			#print "Yesterday was hot."
		#else:
			#print "Yesterday was cold."
		#print "The probability of it being hot today is:", transition[0, yesterday]
		#binomial, one sample
		today = 1 - np.random.binomial(1, transition[0, yesterday])
		#This is dumb, but "hot" was supposed to be 0 for some dumb reason.
		#So when we return a 1 for 'success', we need to change it to a 0
		#So that it means 'hot'
		#I get the impression this part was not written out super duper well
		#if today == 0:
			#print "Today is hot."
		#else:
			#print "Today is cold."
		days.append(today)
		#print "This is the record of days:"
		#print days
		#print "\n"
		
	return days


# Problem 3
def four_state_forecast(days):
	"""Run a simulation for the weather over the specified number of days,
	with mild as the starting state, using the four-state Markov chain.
	Return a list containing the day-by-day results, not including the
	starting day.

	Examples:
		>>> four_state_forecast(3)
		[0, 1, 3]
		>>> four_state_forecast(5)
		[2, 1, 2, 1, 1]
	"""
	#Construct our transition matrix
	transition = np.array([
	[.5,.3,.1,0],
	[.3,.3,.3,.3],
	[.2,.3,.4,.5],
	[0.,.1,.2,.2]])
	
	#define each temperature corresponding to the column it's at
	hot = 0
	mild = 1
	cold = 2
	freezing = 3
	
	#Assume the starting day is mild
	yesterday = mild
	#Don't know next day yet
	today = None
	#Get ready to record the days
	days_list = list()
	
	for d in range(1, days+1):
		#uncomment this stuff for debugging.
		"""
		print "Day %d"%d
		if yesterday == hot:
			print "Yesterday was hot."
		elif yesterday == mild:
			print "Yesterday was mild."
		elif yesterday == cold:
			print "Yesterday was cold."
		elif yesterday == freezing:
			print "Yesterday was freezing."
		temp_probabilities = transition[:,yesterday]
		print "There is a", temp_probabilities[0], "probability of it being hot." 
		print "There is a", temp_probabilities[1], "probability of it being mild."
		print "There is a", temp_probabilities[2], "probability of it being cold."
		print "There is a", temp_probabilities[3], "probability of it being freezing."
		multinomial_result = np.random.multinomial(1, temp_probabilities)
		print "This is the multinomial result:"
		print multinomial_result
		index_of_the_1 = np.argmax(multinomial_result)
		print index_of_the_1,"was the index 1 was at"
		if index_of_the_1 == hot:
			print "Today will be a hot day"
		elif index_of_the_1 == mild:
			print "Today will be a mild day"
		elif index_of_the_1 == cold:
			print "Today will be a cold day"
		elif index_of_the_1 == freezing:
			print "Today will be a freezing day"
		today = index_of_the_1
		days_list.append(today)
		yesterday = today
		print "This is the record of days:"
		print days_list
		print "\n"
		"""
		#transition[:,yesterday] -- temperature probability distribution for today
		#np.random.mulitinomial(1, xxxx) -- get an array with a 1 the index of the probability corresponding to the event
		#np.argmax(xxxx) -- get the index, which would correspond to hot/cold/mild/etc
		#See the string for more detailed breakdown
		today = np.argmax(np.random.multinomial(1, transition[:,yesterday]))
		days_list.append(today)
		yesterday = today
		
	return days_list


# Problem 4
def steady_state(A, tol=1e-12, N=40):
	"""Compute the steady state of the transition matrix A.

	Inputs:
		A ((n,n) ndarray): A column-stochastic transition matrix.
		tol (float): The convergence tolerance.
		N (int): The maximum number of iterations to compute.

	Raises:
		ValueError: if the iteration does not converge within N steps.

	Returns:
		x ((n,) ndarray): The steady state distribution vector of A.
	"""
	m,n = A.shape
	#Make sure the matrix is square
	if m != n:
		raise ValueError("%r is not a square matrix"%A)
	#Make sure it is column stochastic
	if not np.allclose(A.sum(axis=0), np.ones(n)):
		raise ValueError("%r is is not a column stochastic matrix"%A) 
		
	#Generate a random state vector x0
	x0 = np.random.random(n)
	#make sure that it sums to 1
	x0 /= x0.sum()
	
	#Calculate x^{k+1} = Ax^k until || x^{k+1} - x^{k} || < tol
	k = 0
	x_k = x0
	x_kp1 = np.dot(A, x_k)
	while(np.linalg.norm(x_kp1 - x_k) >= tol):
		k += 1
		if k > N: #if k exceeds N
			raise ValueError("%r does not converge"%A)
			
		x_k = x_kp1
		x_kp1 = np.dot(A, x_k)
		
	#Return the approximate steady state distribution		
	return x_kp1


# Problems 5 and 6
class SentenceGenerator(object):
	"""Markov chain creator for simulating bad English.

	Attributes:
		Transition_Matrix -- The Markov Transition Matrix from word to word
		states -- list where each entry corresponds the the index of the Transition Matrix
		(what attributes do you need to keep track of?)

	Example:
		>>> yoda = SentenceGenerator("Yoda.txt")
		>>> print yoda.babble()
		The dark side of loss is a path as one with you.
	"""

	def __init__(self, filename):
		"""Read the specified file and build a transition matrix from its
		contents. You may assume that the file has one complete sentence
		written on each line.
		"""
		whole_file = str()
		with open(filename) as training_set:
			sentences = training_set.readlines()
			
		#Step 1
		#keep track of unique words	
		words = set()
		#Go through each sentence
		for sentence in sentences:
			words_in_sentence = sentence.split(" ")
			
			#add each word in the sentence to the set (sets have no duplicates)
			for word in words_in_sentence:
				words.add(word)
		
		number_of_unique_words = len(words)
		
		#Step 2
		#initialize a square array of zeros of the appropriate size
		#to be the transition matrix (remember to account for start and stop) 
		appropriate_dimension = tuple([number_of_unique_words + 2])*2
		the_matrix = np.zeros(appropriate_dimension)
		
		#Step 3
		#Initialize a list of states beginning with "$tart"
		list_of_states = ["$tart"]
		
		#Step 4
		for sentence in sentences:
			#Step 5
			#split the sentence into a list of words
			list_of_words = sentence.split(" ")
			
			#Step 6
			#add each NEW word in the sentence to the list of states
			for word in list_of_words:
				if word not in list_of_states:
					list_of_states.append(word)
			
			#Step 7
			#Convert the list of words into a list of indices
			#indicating which row and column each word corresponds to
			#in the transition matrix
			list_of_indices = list()
			for word in list_of_words:
				list_of_indices.append(list_of_states.index(word))
			
			#Step 8
			#Add 1 to the entry of the transition matrix corresponding to
			#transitioning from the start state to the first word of the sentence
			first_word_index = list_of_states.index(list_of_words[0])
			the_matrix[first_word_index,0] += 1
			
			#Step 9
			#For each consecutive pair (x,y) in the list of words, do:
			for i in range(len(list_of_words)-1): #subtract 1 to prevent overindexing
				x = list_of_words[i]
				y = list_of_words[i+1]
				
				#Step 10
				#Add 1 to the entry of the transition matrix corresponding to
				#transitioning from state x to state y
				x_index = list_of_states.index(x)
				y_index = list_of_states.index(y)
				the_matrix[y_index, x_index] += 1
			
			#Step 11
			#Add 1 to the entry of the matrix corresponding to transitioning
			#from the last word of the sentence to the stop state
			last_word_index = list_of_states.index(list_of_words[-1])
			the_matrix[-1, last_word_index] += 1
			
		#Step 12
		#Make sure the stop state transistions to itself
		the_matrix[-1,-1] = 1

		
		#Step 13
		#Normalize each column by dividing by the column sums
		the_matrix /= the_matrix.sum(axis = 0)
					
		self.Transition_Matrix = the_matrix
		self.states = list_of_states
		return

	def babble(self):
		"""Begin at the start sate and use the strategy from
		four_state_forecast() to transition through the Markov chain.
		Keep track of the path through the chain and the corresponding words.
		When the stop state is reached, stop transitioning and terminate the
		sentence. Return the resulting sentence as a single string.
		"""
		previous_state = 0
		path_of_states = list()
		corresponding_words = list()
		current_state = None
		
		while(current_state != len(self.states)):
			#print "This is the array we will use for the multinomial:"
			#print self.Transition_Matrix[:,previous_state]
			result = np.random.multinomial(1, self.Transition_Matrix[:,previous_state])
			#print "This is the result:"
			#print result
			
			current_state = np.argmax(result)
			
			#print "This is the current state:", current_state
			if current_state != len(self.states) and current_state != 0: #reached "$top"
				path_of_states.append(current_state)
				corresponding_words.append(self.states[current_state])
				
			previous_state = current_state
		
		babble_sentence = " ".join(corresponding_words)
		return babble_sentence
		
		raise NotImplementedError("Problem 6 Incomplete")

if __name__ == "__main__":
	want_to_test_prob1 = False
	want_to_test_prob2 = False
	want_to_test_prob3 = False
	want_to_test_prob4 = False
	want_to_test_prob5 = False
	want_to_test_prob6 = True
	
	
	if want_to_test_prob1:
		for n in range(2,7)[::-1]:
			for throwaway_index in range(5):
				A = random_markov(n)
				print "This is A:"
				print A
				print "This is it's sum along it's columns"
				print A.sum(axis = 0)
				print "\n"
				
	if want_to_test_prob2:
		days_list = np.array(forecast(100))
		print "This was the average of the days_list:"
		print days_list.mean()
		print "There were", len(days_list), "days"
	
	if want_to_test_prob3:
		print four_state_forecast(1)
		print four_state_forecast(100)
		
	if want_to_test_prob4:
		A = np.random.random((3,3))
		try:
			steady_state(A)
		except ValueError as e:
			print "The steady state said %s"%e
			print "So, it correctly rejects non-stochastic matrices"
		else:	
			print "The steady state function accepts bad matrices!"
			
		number_of_trials = raw_input("Enter the number of trials to do on each matrix size : ")
		number_of_trials = int(number_of_trials)
		
		for n in range(2,13):
			print "\n\n\t %dx%d matrices"%(n,n)
			for throwaway_index in range(number_of_trials):
				A = random_markov(n)
				try:
					x = steady_state(A)
				except ValueError:
					print A
					print "Didn't converge.\n\n"
				else:
					print "This is the steady state distribution:"
					print x
					
					y = np.linalg.matrix_power(A, 40)
					if n < 6:
						print "np.linalg.matrix_power(A, 40):"
						print y
					else:
						print "np.linalg.matrix_power(A, 40)[:,0]"
						print y[:,0]
					
					if np.all(x == y[:,0]):
						print "They're exactly the same"
					elif np.allclose(x, y[:,0]):
						print "They're close enough."
					else:
						raw_input("These ones are not close.\nPress enter to continue")
					 
					print "\n"
					
	
	if want_to_test_prob5 or want_to_test_prob6:
		X = SentenceGenerator("Green Eggs and Ham.txt")
		print X.Transition_Matrix
		if want_to_test_prob6:
			for throaway_index in range(50):
				print X.babble()
				
			X = SentenceGenerator("tswift1989.txt")
			print X.Transition_Matrix
			print X.states
			for throwaway_index in range(50):
				print X.babble()
			X = SentenceGenerator("yoda.txt")
			for throwaway_index in range(50):
				print X.babble()
			X = SentenceGenerator("trump.txt")
			for throwaway_index in range(50):
				print X.babble()
