# linked_lists.py
"""Volume II Lab 4: Data Structures 1 (Linked Lists)
Logan Schelly
Math 345
"""
from collections import deque
#note: this is for problem 7
from random import randint
#for testing

# Problem 1
class Node(object):
    """A basic node class for storing data.
    
    Attributes: value -- a string, int, float or long.
    """
    #Modify the constructor doc string to document revisions
    def __init__(self, data):
        """Initializes the attribute 'value' to 'data'
        Data must be either a float, an int, or a string.
        """
        if isinstance(data, float):
            pass
        elif isinstance(data, int):
            pass
        elif isinstance(data, str):
            pass
        else: #we do not have a float, int, long, or string
            message = "The argument must be of type float, int, long, or str."
            message += "It is of type%s"%(type(data))
            raise TypeError(message)
            
        #Store the data in the node's value attribute
        self.value = data


class LinkedListNode(Node):
    """A node class for doubly linked lists. Inherits from the 'Node' class.
    Contains references to the next and previous nodes in the linked list.
    """
    def __init__(self, data):
        """Store 'data' in the 'value' attribute and initialize
        attributes for the next and previous nodes in the list.
        """
        Node.__init__(self, data)       # Use inheritance to set self.value.
        #When first initialized, a node with have no predecessors or successors.
        self.next = None
        self.prev = None


class LinkedList(object):
    """Doubly linked list data structure class.

    Attributes:
        head (LinkedListNode): the first node in the list.
        tail (LinkedListNode): the last node in the list.
        number of nodes(int): the number of nodes in the list.
        
    Methods:
        append() - put new node at end
        find() - find a node
        remove() - remove a node
        insert() - insert a new node before a give node
    """
    def __init__(self):
        """Initialize the 'head' and 'tail' attributes by setting
        them to 'None', since the list is empty initially.
        
        Also make the length of the list 0
        """
        self.head = None
        self.tail = None
        self.number_of_nodes = 0

    def append(self, data):
        """Append a new node containing 'data' to the end of the list."""
        # Create a new node to store the input data.
        new_node = LinkedListNode(data)
        
        if self.head is None: #the list is empty.
            # The new node become both head and tail.
            self.head = new_node
            self.tail = new_node
        
        else: # the list is not empty.
            #Place a new node after the tail.
            self.tail.next = new_node               # tail --> new_node
            new_node.prev = self.tail               # tail <-- new_node
            
            #update the tail to point at the new node at the end.
            self.tail = new_node
            
        #regardless of whether we were empty, update the number of nodes.
        self.number_of_nodes += 1
        
    #Problem 2        
    def find(self, data):
        """
        Returns the first node in the list containing "data"
        If no such node exists, raises a ValueError
        """
        #Check to make sure we actually have a head
        if self.head == None:
            raise ValueError("List has no head. Can't start search for %r."%data)
            
        
        #Start at the head
        current_node = self.head
        
        #Iterate through all the nodes
        while(current_node != None):
        
            #Check if the node you are on has 'data' as it's value.
            value_is_data = (current_node.value == data)
            
            #If the node does have 'data', return the node
            if value_is_data:
                return current_node
            
            #Check to see if we're at the tail.
            if self.tail is current_node:
                raise ValueError("Searched the list from head to tail.  Could not find %r"%data)
        
            #If not, move to the next node
            current_node = current_node.next
        


    # Problem 3
    def __len__(self):
        """Return the number of nodes in the list."""
        return self.number_of_nodes

    # Problem 3
    def __str__(self):
        """
        String representation: the same as a standard Python list.
        """
        #Put a left-open bracket
        string_rep = "["
        #iterate through the whole list, starting at the head
        current_node = self.head
        
        while (current_node != None):
            #Put the raw representation of the data
            string_rep += "%r" % (current_node.value)
                
            #put a comma and a space if the current node is not the tail
            if (current_node is not self.tail):
                string_rep += ", "
            
            #update to the next node
            current_node = current_node.next
            
        #When we're done with all the nodes' values, put a closing bracket
        string_rep += "]"
        
        #return the string representation
        return string_rep

    # Problem 4
    def remove(self, data):
        """
        Remove the first node in the list containing 'data'. Return nothing.

        Raises:
            ValueError: if the list is empty, or does not contain 'data'.
        """
        #first, check if the list is empty.
        if(len(self) == 0):
            raise ValueError("The list is empty.  Impossible to remove %r"%data)
    
        #If the list is not empty,
        try:
            #try to find the first node containing the data, and call it x
            x = self.find(data)
        except ValueError as e:
            #if the data is not in the list, raise a ValueError
            raise ValueError("Tried to find %r.  find() says: %s"%(data, e))
        
        #If the node is both head and tail
        if len(self) == 1:
            #Now have an empty list.  No head or tail.
            self.head = None
            self.tail = None
            
        #If the node is the head.
        elif x is self.head:
            #update the head
            self.head = self.head.next
            #make sure the head has no previous node
            self.head.prev = None
            
        #If the node is the head
        elif x is self.tail:
            #update the tail
            self.tail = self.tail.prev
            #make sure the tail has no next node
            self.tail.next = None
            
        #normal case
        else:    
            # make the node before x point to the node after x
            node_before_x = x.prev
            node_before_x.next = x.next
            #make the node after x point to the node before x
            node_after_x = x.next
            node_after_x.prev = x.prev
        
        #in all cases, we decrement the amount of nodes in the list
        self.number_of_nodes -= 1
        return #nothing

    # Problem 5
    def insert(self, data, place):
        """
        Insert a node containing 'data' immediately before the first node
        in the list containing 'place'. Return nothing.

        Raises:
            ValueError: if the list is empty, or does not contain 'place'.
        """
        #find the node with value 'place'.  Call it x.
        try:
            x = self.find(place)
        except ValueError as e:
            raise ValueError("find() says: " + str(e))
            
        #make a new node with data as it's value
        new_node = LinkedListNode(data)
        
        #If we need to insert before the head.
        if x is self.head:
            #make the old head point to the new node
            self.head.prev = new_node
            #make new node point to the old head
            new_node.next = self.head
            #make the new node the new head
            self.head = new_node
            
        #If we need to insert anywhere else.
        else:
            #make the node point to x and point to the node before x
            new_node.prev = x.prev
            new_node.next = x
            #make x and the node that was after x point to the new node
            x.prev.next = new_node
            x.prev = new_node
            
        #finally, increment the number of nodes
        self.number_of_nodes += 1
        return #nothing


# Problem 6: Write a Deque class.
class Deque(LinkedList):
    """
    Deque data structure class.  Inherits from Linked List
    
    Attributes:
        head (LinkedListNode): the first node in the list.
        tail (LinkedListNode): the last node in the list.
        number of nodes(int): the number of nodes in the list.
        
    Methods:
        append(): put a new node at the end of the list  (inherited from linked List)
        pop(): remove the last node in the list and return it's data
        appendleft(): put a new node at the beginning of the list
        popleft(): remove the head of the list and return it's data
    
    Disabled Methods:
        remove()
        insert() 
    """
    def __init__(self):
        """
        Does what LinkedList.__init__ does:
        Initialize the 'head' and 'tail' attributes by setting
        them to 'None', since the list is empty initially.
        
        Make length of the list 0
        """
        LinkedList.__init__(self) #inherits from LinkedList object
        #no need for additional attributes
        
    def pop(self):
        """
        Remove the last node in the list, and return it's data
        """
        #get the data of the last node
        old_tail_data = self.tail.value
        
        #check to see if the tail and head are equal.
        if self.head is self.tail:
            #Deque should have no tail and no head after this.
            self.head = None
            self.tail = None
  
        else:
            #update the tail to be it's predecessor.
            self.tail = self.tail.prev
            #cut off the old tail
            self.tail.next = None
            #since nothing is pointing to the old tail, it gets gc'd
        
        #decrement the number of things in the list
        self.number_of_nodes -= 1
        
        return old_tail_data
        
    def appendleft(self, data):
        """
        Insert a new node containing data at the beginning of the list.
        """
        #If there's already stuff in the Deque
        if (len(self) != 0) :
            #insert this data right before the head.
            LinkedList.insert(self, data, self.head.value)

        else: #append can handle adding to an empty list.
            self.append(data)
            
        return #nothing
        
    def popleft(self):
        """
        Remove the first node in the list and return its data.
        """
        #get the data in the head
        head_data = self.head.value
        
        #remove the head
        if len(self) != 0 :
            LinkedList.remove(self, self.head.value)
            
        #return the data that was in the old head
        return head_data
        
    def remove(*args, **kwargs):
        """
        Disabled for Deque object
        """
        raise NotImplementedError("Use pop() or popleft() for removal")
        
    def insert(*args, **kwargs):
        """
        Disabled for Deque object
        """
        raise NotImplementedError("Use append() or appendleft() for insertion")


# Problem 7
def prob7(infile, outfile):
    """Reverse the file 'infile' by line and write the results to 'outfile'."""
    #gonna use the deque type from collections.  See import in beginnning
    lines = deque()
    with open(infile, 'r') as f:
        for i in f.readlines():
            lines.append(i)
    with open(outfile, 'w') as f:
        lastline = lines.pop()
        if lastline[-1] != "\n": #check to see if we had a newline on the last line
            lastline += "\n"
        f.write(lastline)
        while (len(lines) > 1):
            f.write(lines.pop())
        firstline = lines.pop();
        if firstline[-1] == "\n":
            firstline = firstline[:-1]
        f.write(firstline)
    return
    
#Testing
def prob1_test():
    print("\nTesting problem 1 ...\n")
    string = "Hello World"
    i = 0
    li = 53
    fl = 3.1415
    wrong_type = [string, i, li, fl]
    
    string_node = Node(string)
    print("The Node object plays well with strings: " + str(string == string_node.value))
    i_node = Node(i)
    print("The Node object plays well with ints:    " + str(i == i_node.value))
    li_node = Node(li)
    print("The Node object plays well with longs:   " + str(li == li_node.value))
    fl_node = Node(fl)
    print("The Node object plays well with floats:  " + str(fl == fl_node.value))
    
    print("\nNow let's try to pass in something improper...\n")
    try:
        blow_up = Node(wrong_type)
    except TypeError as e:
        print("The constructor correctly displayed the message:\n" + str(e))
    else:
        print("The constructor took the bad data!!!")
    finally:
        print("\nAll done :)\n")
        
def prob2_test():
    print("Making a list with 1,3,5,7,9 in it")
    l = LinkedList()
    for i in [1,3,5,7,9]:
        l.append(i)
    
    print("\nSearching the list for the integer 5...")
    node =  l.find(5)
    print("The find() method returned a node, and node.value == %r"%node.value)
    
    print("\nNow finding the integer 10")
    try:
        node = l.find(10)
    except ValueError as e:
        print ("The find() method threw a ValueError with the message %s"%e)
    
    print("\nNow searching an empty list for the integer 2.")
    empty_l = LinkedList()
    try:
        node = empty_l.find(2)
    except ValueError as e:
        print ("The find() method threw a ValueError with the message %s"%e)
    
def len_test():
    l = LinkedList()
    print("calculating the length of the list %r"%([1,3,5,7]))
    for i in [1,3,5,7]:
        print ("append %r"%i)
        l.append(i)
        print("list is of length " , len(l))
    
def string_test():
    l = LinkedList()
    print("Printing the LinkedList at every stage of appending")
    for i in [1,3,5,7,'apple']:
        print("append %r"%i)
        l.append(i)
        print("The list's string format is: ", l)
        
def remove_test():
    print("Constructing l1 from [1, 3, 5, 7, 9]")
    l1 = LinkedList()
    for i in [1, 3, 5, 7, 9]:
        l1.append(i)
        
    print("This is l1: " , l1)
    
    print("\nNow removing 5,1, and 9")
    for i in [5,1,9]:
        print("Removing " , i)
        l1.remove(i)
        print("Now this is l1: ", l1)
        print("The length of l1 is ", len(l1))
        
    print("\nConstructing l2 from [2,4,6,8]\n")
    l2 = LinkedList()
    for i in [2,4,6,8]:
        print("Adding ", i)
        l2.append(i)
        print("Now this is l2:", l2)
    
    print("\nNow trying to remove 10 from l2")
    try:
        l2.remove(10)
    except ValueError as e:
        print("It threw a ValueError: %s"%e)
    else:
        print("It didn't throw a value error!")
    finally:
        print("this is now l2: ", l2)
        print("The length of l2 is:", len(l2))
        
    print("\nMaking an empty LinkedList, l3")
    l3 = LinkedList()
    print("This is l3:" , l3)
    print("The length of l3 is:", len(l3))
    
    print("\nNow trying to remove 10 from l3")
    try:
        l3.remove(10)
    except ValueError as e:
        print("It threw a ValueError: %s"%e)
    else:
        print("It failed to throw a ValueError!!!")
    finally:
        print("This is now l3:", l3)
        print("The length of l3 is:", len(l3))
    return

def insert_test():
    l1 = LinkedList()
    for i in [1,3,7]:
        l1.append(i)
    print("l1:", l1)
    print("\n")
    
    for i in [[7,7], [5,7], [3,2], [10,10], ["new head", 1]]:
        print("Trying l1.insert(%r, %r)"%(i[0], i[1]))
        try:
            l1.insert(i[0], i[1])
        except ValueError as e:
            print("The insert method threw the ValueError: %s"%e)
        else:
            print("The insert method did not throw a ValueError.")
        finally:
            print("This is now l1:" , l1)
            print("The length of l1 is ", len(l1), "\n")
            
def test_Deque():
    print("Appending the data in this list to the left: [1,2,3,4]")
    x =  Deque()
    print("This is x: " , x)
    for i in [1,2,3,4]:
        x.appendleft(i)
        print("This is now our Deque: ", x)
        
    print("\n popping right until I can't anymore")
    while(len(x) != 0):
        print("popping this off the deck: ", x.pop())
        print("This is now the deque: ", x)
    
    random_list = [randint(0,500) for i in range(10)]
    print("\nNow I will append this list to the right: ", random_list)
    for i in random_list:
        print("adding this to the right: ", i)
        x.append(i)
        print("this is now x: " , x)
    
    print("\n Now I will pop left until I can't anymore")
    while (len(x) != 0):
        print("popping this off the deck: ", x.popleft())
        print("this is now x: ", x)
        
        
if __name__ == "__main__":
    prob7("english.txt", "backwards_english.txt")
    
