# bfs_kbacon.py
"""Volume 2A: Breadth-First Search (Kevin Bacon).
Logan Schelly
Math 321
24 Oct 2016
"""
import numpy as np
from collections import deque
import networkx as nx
from matplotlib import pyplot as plt


# Problems 1-4: Implement the following class
class Graph(object):
	"""A graph object, stored as an adjacency dictionary. Each node in the
	graph is a key in the dictionary. The value of each key is a list of the
	corresponding node's neighbors.

	Attributes:
		dictionary: the adjacency list of the graph.
	"""

	def __init__(self, adjacency):
		"""Store the adjacency dictionary as a class attribute."""
		self.dictionary = adjacency

	# Problem 1
	def __str__(self):
		"""String representation: a sorted view of the adjacency dictionary.

		Example:
			>>> test = {'A':['B'], 'B':['A', 'C',], 'C':['B']}
			>>> print(Graph(test))
			A: B
			B: A; C
			C: B
		"""
		string_rep = str()
		for x in sorted(self.dictionary):
			string_rep += x + ": " + "; ".join(self.dictionary[x])+ "\n"
		return string_rep

	# Problem 2
	def traverse(self, start, debugging=False):
		"""Begin at 'start' and perform a breadth-first search until all
		nodes in the graph have been visited. Return a list of values,
		in the order that they were visited.

		Inputs:
			start: the node to start the search at.

		Returns:
			the list of visited nodes (in order of visitation).

		Raises:
			ValueError: if 'start' is not in the adjacency dictionary.

		Example:
			>>> test = {'A':['B'], 'B':['A', 'C',], 'C':['B']}
			>>> Graph(test).traverse('B')
			['B', 'A', 'C']
		"""
		V = set() #already visited
		L = list() #list of visited nodes in order of visitation
		N = deque() #nodes yet to vist
		M = set() #nodes already placed in N
		
		if start not in self.dictionary.keys():
			raise ValueError("%r is not in the adjacency dicionary"%start)
		
		current = start
		
		if debugging:
			print "This is the starting node we will visit:", current
			
		V.add(current)
		L.append(current)
		
		if debugging:
			print "This is the set of nodes we have visited:"
			print V
			print "This is the ordered list of nodes we have visited:"
			print L
			
			print "Adding %s's neighbors to the deque of nodes to visit"%current
			
		for neighbor in self.dictionary[current]:
			N.append(neighbor)
			M.add(neighbor)
			
		if debugging:
			print "This is our visit queue"
			print N
			print "This is the set of nodes we have marked as in our visit queue:"
			print M
		
		while(len(N)>0):#when len(N) == 0, there are no more nodes to visit
			current =  N.popleft()
			V.add(current)
			L.append(current)
			
			if debugging:
				print "\nThis is the node we are checking:", current
				print "These are it's neighbors:", self.dictionary[current]
				print "This is the set of nodes we have visited:"
				print V
				print "This is the ordered list of nodes we have visited:"
				print L
				print "Iterating through the neighbors..."
				
			for neighbor in self.dictionary[current]:
				if neighbor in V:
					if debugging:
						print "We already visited %r"%(neighbor)
				elif neighbor in M:
					if debugging:
						print "%r is already in the queue!"%neighbor
				elif neighbor not in M and neighbor not in V:
					if debugging:
						print "%r is not in the queue"%neighbor
						print "%r is not already visited."%neighbor
						print "We will add it to the queue!"
					N.append(neighbor)
					M.add(neighbor)
					
			if debugging:
				print "This is now our queue:"
				print N
				print "This is the set of marked nodes:"
				print M
				
		if debugging:
			print "\nThis is the queue of nodes to visit:"
			print N
			print "It's empty!"
			print "We will now return the list of nodes we visited:"
			print L
			print "\n"
		
		return L

	# Problem 3 (Optional)
	def DFS(self, start):
		"""Begin at 'start' and perform a depth-first search until all
		nodes in the graph have been visited. Return a list of values,
		in the order that they were visited. If 'start' is not in the
		adjacency dictionary, raise a ValueError.

		Inputs:
			start: the node to start the search at.

		Returns:
			the list of visited nodes (in order of visitation)
		"""
		raise NotImplementedError("Problem 3 Incomplete")

	# Problem 4
	def shortest_path(self, start, target):
		"""Begin at the node containing 'start' and perform a breadth-first
		search until the node containing 'target' is found. Return a list
		containg the shortest path from 'start' to 'target'. If either of
		the inputs are not in the adjacency graph, raise a ValueError.

		Inputs:
			start: the node to start the search at.
			target: the node to search for.

		Returns:
			A list of nodes along the shortest path from start to target,
				including the endpoints.

		Example:
			>>> test = {'A':['B', 'F'], 'B':['A', 'C'], 'C':['B', 'D'],
			...		 'D':['C', 'E'], 'E':['D', 'F'], 'F':['A', 'E', 'G'],
			...		 'G':['A', 'F']}
			>>> Graph(test).shortest_path('A', 'G')
			['A', 'F', 'G']
		"""
		if start == target:
			return [start]
		if target not in self.dictionary.keys():
			raise ValueError("%r is not in the adjacency dicionary"%start)
		
		V = set() #already visited
		P = dict() #dictionary for paths taken to the nodes in V
		N = deque() #nodes yet to vist
		M = set() #nodes already placed in N
	
		V.add(start)
		
		for neighbor in self.dictionary[start]:
			if neighbor == target:
				return [start, target]
			N.append(neighbor)
			M.add(neighbor)
			P[neighbor] = start
			
		found_target = False	
		while(not found_target):
			current = N.popleft()
			V.add(current) #visited current
			
			if target in self.dictionary[current]:
				found_target = True
				P[target] = current
				break
				
			for neighbor in self.dictionary[current]:
				if (neighbor not in V) and (neighbor not in M):
					N.append(neighbor)
					M.add(neighbor)
					P[neighbor] = current
				
		backward_path = list()
		vertex = target #start at target and work backwards
		while(vertex != start):
			backward_path.append(vertex)
			vertex = P[vertex]
		forward_path = backward_path[::-1]
		
		return [start] + forward_path


# Problem 5: Write the following function
def convert_to_networkx(dictionary):
	"""Convert 'dictionary' to a networkX object and return it."""
	networkx_object = nx.Graph()
	for vertex in dictionary.keys():
		for neighbor in dictionary[vertex]:
			networkx_object.add_edge(vertex, neighbor)
	return networkx_object


# Helper function for problem 6
def parse(filename="movieData.txt"):
	"""Generate an adjacency dictionary where each key is
	a movie and each value is a list of actors in the movie.
	"""

	# open the file, read it in, and split the text by '\n'
	with open(filename, 'r') as movieFile:
		moviesList = movieFile.read().split('\n')
	graph = dict()

	# for each movie in the file,
	for movie in moviesList:
		# get movie name and list of actors
		names = movie.split('/')
		title = names[0]
		graph[title] = []
		# add the actors to the dictionary
		for actor in names[1:]:
			graph[title].append(actor)

	return graph


# Problems 6-8: Implement the following class
class BaconSolver(object):
	"""Class for solving the Kevin Bacon problem."""

	# Problem 6
	def __init__(self, filename="movieData.txt"):
		"""Initialize the networkX graph and with data from the specified
		file. Store the graph as a class attribute. Also store the collection
		of actors in the file as an attribute.
		"""
		self.graph = nx.Graph()
		self.collection_of_actors = set()
		
		adjacency_dictionary = parse(filename)
		
		#iterate through all the movies
		for movie in adjacency_dictionary.keys():
					
			actors_in_movie = set(adjacency_dictionary[movie])
			#add the actors in the movie to the collection of all actors
			self.collection_of_actors.update(actors_in_movie)
			
			#for each movie, iterate through all the actors
			for actor in adjacency_dictionary[movie]:
				#The actor's coworkers would be all the movie's actors except himself
				coworkers = actors_in_movie.difference([actor])
				#iterate through all the actor's coworkers
				for coworker in coworkers:
					#make an edge between the actor and the coworker
					self.graph.add_edge(actor, coworker)


	# Problem 6
	def path_to_bacon(self, start, target="Bacon, Kevin"):
		"""Find the shortest path from 'start' to 'target'."""
		if start not in self.collection_of_actors:
			raise ValueError("%s is not an actor in the database"%start)
		elif target not in self.collection_of_actors:
			raise ValueError("%s is not an actor in the database"%target)
		else:
			return nx.shortest_path(self.graph, start, target)

	# Problem 7
	def bacon_number(self, start, target="Bacon, Kevin"):
		"""Return the Bacon number of 'start'."""
		return len(self.path_to_bacon(start, target)) - 1

	# Problem 7
	def average_bacon(self, target="Bacon, Kevin"):
		"""Calculate the average Bacon number in the data set.
		Note that actors are not guaranteed to be connected to the target.

		Inputs:
			target (str): the node to search the graph for
		"""
		all_bacon_numbers = list()
		actors_not_connected = set()
		for actor in self.collection_of_actors:
			try:
				try:
					all_bacon_numbers.append(self.bacon_number(actor))
				except nx.NetworkXError:
					#some actors have no connections?
					actors_not_connected.add(actor)
					
			except nx.NetworkXNoPath as e:
				actors_not_connected.add(actor)
		
		average_bacon_number = sum(all_bacon_numbers)/(len(all_bacon_numbers) * 1.0)
		return average_bacon_number, len(actors_not_connected)
		
if __name__ == "__main__":
	want_to_test_prob_1 = False
	want_to_test_prob_2 = False
	want_to_test_prob_3 = False
	want_to_test_prob_4 = False
	want_to_test_prob_5 = True
	want_to_test_prob_6 = True
	
	if want_to_test_prob_1 or want_to_test_prob_2 or want_to_test_prob_3 or want_to_test_prob_4:
		test1 = {'A':['B'], 'B':['A', 'C',], 'C':['B']}
		graph1 = Graph(test1)
		test2 = {'A':['B', 'C', 'D', 'E'], 'B':['C', 'A'], 'C':['B', 'A', 'D'], 'D':['C', 'A'], 'E':['A']}
		graph2 = Graph(test2)
		
		if want_to_test_prob_1:
			print graph1
			print graph2
		
		if want_to_test_prob_2:
			graph1.traverse('A', debugging = True)
			graph2.traverse('C', debugging = True)
		
		test3 = dict()
		test4 = dict()
		if want_to_test_prob_3 or want_to_test_prob_4:
			import string
			for c in string.ascii_uppercase:
				neighborhood = list()
				if ord(c) > ord('A'):
					neighborhood.append(chr(ord(c) -1))
				if ord(c) < ord('Z'):
					neighborhood.append(chr(ord(c)+1))
				test3[c] = neighborhood
				
			for c in string.ascii_uppercase[:10]:
				neighborhood = list()
				if ord(c) > ord('A'):
					neighborhood.append(chr(ord(c) -1))
				if ord(c) < ord('A')+10:
					neighborhood.append(chr(ord(c)+1))
				test4[c] = neighborhood
			graph4 = Graph(test4)
		
		if want_to_test_prob_3:
			print graph2.shortest_path('C', 'E')
			print graph3
			print graph3.shortest_path('A', 'Z')
			
		if want_to_test_prob_4:
			for t in [test1, test2, test3, test4]:
				x = convert_to_networkx(t)
				print x.nodes()
				print x.edges()
				nx.draw(x)
				plt.show()
				
	if want_to_test_prob_5 or want_to_test_prob_6:
		x = BaconSolver()
		actors = list()
		actors.append("Downey Jr., Robert")
		actors.append("Bale, Christian")
		actors.append("Neeson, Liam")
		actors.append("Eckhart, Aaron")
		actors.append("Offerman, Nick")
		actors.append("Carell, Steve")
		actors.append("Lawrence, Jennifer")
		actors.append("Portman, Natalie")
		actors.append("Kunis, Mila")
		actors.append("Smith, Maggie")
		actors.append("Witherspoon, Reese")
		actors.append("O'Farrel, Conor")
		for actor in actors:
			try:
				if want_to_test_prob_5:
					print x.path_to_bacon(actor)
				if want_to_test_prob_6:
					print actor, ":", x.bacon_number(actor), "steps to Bacon, Kevin"
			except ValueError as e:
				print e
				
		if want_to_test_prob_6:
			average_bacon, number_of_loners = x.average_bacon()
			print "Average bacon number:", average_bacon
			print  number_of_loners, "actors are not connected to Kevin Bacon"
				
# =========================== END OF FILE =============================== #
